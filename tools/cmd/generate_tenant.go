package cmd

import (
	"encoding/json"
	"fmt"

	uuid "github.com/satori/go.uuid"
	"github.com/spf13/cobra"
	"gitlab.com/opencontent/stanza-del-cittadino/apkappa-payment-proxy/server/models"
)

var generateTenantCmd = &cobra.Command{
	Use:   "tenant",
	Short: "A brief description",
	Long:  `A longer description.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Generated tenant json:")
		fmt.Println()

		endpoint, err := cmd.Flags().GetString("endpoint")
		if err != nil {
			panic(err)
		}
		production, err := cmd.Flags().GetBool("production")
		if err != nil {
			panic(err)
		}
		code, err := cmd.Flags().GetString("code")
		if err != nil {
			panic(err)
		}
		consumer, err := cmd.Flags().GetString("consumer")
		if err != nil {
			panic(err)
		}
		iuv_aux, err := cmd.Flags().GetString("iuv_aux")
		if err != nil {
			panic(err)
		}
		iuv_app, err := cmd.Flags().GetString("iuv_app")
		if err != nil {
			panic(err)
		}
		iuv_consumer, err := cmd.Flags().GetString("iuv_consumer")
		if err != nil {
			panic(err)
		}
		key, err := cmd.Flags().GetString("key")
		if err != nil {
			panic(err)
		}

		tenant := models.Tenant{
			ID:          uuid.NewV4().String(),
			Active:      true,
			Endpoint:    endpoint,
			Production:  production,
			Code:        code,
			Consumer:    consumer,
			IUVAux:      iuv_aux,
			IUVApp:      iuv_app,
			IUVConsumer: iuv_consumer,
			Key:         key,
		}

		bytes, err := json.MarshalIndent(tenant, "", "  ")
		if err != nil {
			panic(err)
		}

		fmt.Println(string(bytes))
		fmt.Println()
	},
}

func init() {
	generateCmd.AddCommand(generateTenantCmd)

	generateTenantCmd.Flags().StringP("endpoint", "e", "https://test.hypersic.net/wspagopa/pagopaservice.asmx", "Endpoint del tenant")
	generateTenantCmd.Flags().Bool("production", false, "Modalità production?")
	generateTenantCmd.Flags().StringP("code", "c", "RHO", "Ente del tenant")
	generateTenantCmd.Flags().StringP("consumer", "o", "TEST.STANZACIT", "CodiceConsumer del tenant")
	generateTenantCmd.Flags().StringP("iuv_aux", "x", "3", "IUV AuxDigit del tenant")
	generateTenantCmd.Flags().StringP("iuv_app", "a", "01", "IUV ApplicationCode del tenant")
	generateTenantCmd.Flags().StringP("iuv_consumer", "n", "30", "IUV ConsumerCode del tenant")
	generateTenantCmd.Flags().StringP("key", "k", "00893240150", "SecurityKey del tenant")
}
