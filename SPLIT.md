# Analisi split

    Code
    Quota -> Amount
    Tipologia -> Kind -> CAP (Capitolo) / ACC (Accertamento)
    ChiaveUnivoca -> Key

---

## BILANCIO FISSO

PROXY SERVICE

    {
      "split": [
        {
          "split_code": "c_1",
          "split_amount": "16.00",
          "split_type": "CAP",
          "split_key": "c_1",
        },
        {
          "split_code": "a_1",
          "split_amount": "0.50",
          "split_type": "ACC",
          "split_key": "a_1",
        }
      ]
    }

PAYMENT - CREATION_PENDING

    {
      "split": []
    }

PAYMENT - PAYMENT_PENDING

    {
      "split": [
        {
          "code": "c_1",
          "amount": "16.00",
          "meta": {
            "split_type": "CAP",
            "split_key": "c_1"
          }
        },
        {
          "code": "a_1",
          "amount": "0.50",
          "meta": {
            "split_type": "ACC",
            "split_key": "a_1",
          }
        }
      ]
    }

## BILANCIO VARIABILE 1

PROXY SERVICE

    {
      "split": [
        {
          "split_code": "c_1",
          "split_amount": "16.00",
          "split_type": "CAP",
          "split_key": "c_1",
        },
        {
          "split_code": "a_1",
          "split_amount": "0.50",
          "split_type": "ACC",
          "split_key": "a_1",
        }
      ]
    }

PAYMENT - CREATION_PENDING

    {
      "split": [
        {
          "code": "c_1",
          "amount" : "14.00",
          "meta": {}
        },
        {
          "code": "a_1",
          "amount": "2.50",
          "meta": {}
        }
      ]
    }

PAYMENT - PAYMENT_PENDING

    {
      "split": [
        {
          "code": "c_1",
          "amount": "14.00",
          "meta": {
            "split_type": "CAP",
            "split_key": "c_1"
          }
        },
        {
          "code": "a_1",
          "amount": "2.50",
          "meta": {
            "split_type": "ACC",
            "split_key": "a_1",
          }
        }
      ]
    }

## BILANCIO VARIABILE 2

PROXY SERVICE

    {
      "split": [
        {
          "split_code": "c_1",
          "split_amount": "16.00",
          "split_type": "CAP",
          "split_key": "c_1",
        },
        {
          "split_code": "a_1",
          "split_amount": "0.50",
          "split_type": "ACC",
          "split_key": "a_1",
        }
      ]
    }

PAYMENT - CREATION_PENDING

    {
      "split": [
        {
          "code": "c_1",
          "amount" : null,
          "meta": {}
        },
        {
          "code": "a_1",
          "amount": "0.50",
          "meta": {}
        }
      ]
    }

PAYMENT - PAYMENT_PENDING

    {
      "split": [
        {
          "code": "a_1",
          "amount": "0.50",
          "meta": {
            "split_type": "ACC",
            "split_key": "a_1",
          }
        }
      ]
    }

---
