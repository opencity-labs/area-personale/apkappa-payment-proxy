FROM golang:1.21-bookworm AS builder

ENV TZ=Europe/Rome

RUN apt-get update \
  && apt-get install -y --no-install-recommends ca-certificates

RUN update-ca-certificates

WORKDIR $GOPATH/src/apkappa-payment-proxy

COPY . .

RUN go mod download
RUN go mod verify

RUN go build -a -v -o /go/bin/apkappa-payment-proxy

###

FROM debian:bookworm-slim

ARG HTTP_PORT="8000"
ENV HTTP_PORT=$HTTP_PORT

# ARG BASEPATH="/payment-proxy/apkappa/"
# ENV BASEPATH=$BASEPATH

ENV TZ=Europe/Rome

RUN apt-get update \
  && apt-get install -y --no-install-recommends curl

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

COPY --from=builder /go/bin/apkappa-payment-proxy /go/bin/apkappa-payment-proxy

CMD ["/go/bin/apkappa-payment-proxy"]

EXPOSE $HTTP_PORT

# HEALTHCHECK --interval=30s --timeout=5s CMD curl -f http://localhost:$HTTP_PORT$BASEPATHstatus || exit 1
