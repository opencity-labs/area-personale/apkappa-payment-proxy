package server

import (
	"fmt"
	"strconv"
)

func GenerateNoticeCode(sctx *ServerContext, iuvAux, iuvApp, iuvConsumer string) (string, error) {
	noticeCodeStr := iuvAux + iuvApp + iuvConsumer + sctx.GenerateCode()

	noticeCode, err := strconv.Atoi(noticeCodeStr)
	if err != nil {
		return "", err
	}
	checkDigit := noticeCode % 93
	checkDigitStr := fmt.Sprintf("%02d", checkDigit)
	if err != nil {
		return "", err
	}

	return noticeCodeStr + checkDigitStr, nil
}

func GetIuvFromNoticeCode(noticeCode string) string {
	return noticeCode[1:]
}
