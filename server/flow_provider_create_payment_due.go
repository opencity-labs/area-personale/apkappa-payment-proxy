package server

import (
	"strconv"
	"time"

	"github.com/hooklift/gowsdl/soap"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/opencontent/stanza-del-cittadino/apkappa-payment-proxy/server/models"
	"gitlab.com/opencontent/stanza-del-cittadino/apkappa-payment-proxy/server/models/apkappa"
)

type FlowProviderCreatePaymentDue struct {
	Name    string
	Sctx    *ServerContext
	Payment *models.Payment
	Service *models.Service
	Tenant  *models.Tenant
	Request *apkappa.CaricaListaVersamenti
	Client  *soap.Client
	Apkappa apkappa.Service
	Err     error
	Msg     string
	Status  bool
	Result  *apkappa.CaricaListaVersamentiResponse
	IUV     string
}

func (flow *FlowProviderCreatePaymentDue) Exec() bool {
	flow.Name = "ProviderCreatePaymentDue"

	flow.Status = true &&
		flow.preparePayment() &&
		flow.prepareRequest() &&
		flow.doRequest()

	return flow.Status
}

func (flow *FlowProviderCreatePaymentDue) preparePayment() bool {
	paymentDetailSplit := []*models.PaymentDetailSplit{}

	if flow.Service.Splitted && flow.Service.Split != nil {
		// fmt.Println("AAAA")
		// aaaa, _ := json.Marshal(flow.Service.Split)
		// fmt.Println(string(aaaa))

		for _, serviceSplit := range flow.Service.Split {
			serviceSplitAmount := serviceSplit.Amount
			paymentSplit := &models.PaymentDetailSplit{
				Code:   serviceSplit.Code,
				Amount: &serviceSplitAmount,
				Meta: &models.PaymentDetailSplitMeta{
					Type: serviceSplit.Type,
					Key:  serviceSplit.Key,
				},
			}
			paymentDetailSplit = append(paymentDetailSplit, paymentSplit)
		}
	}

	// fmt.Println("BBBB")
	// bbbb, _ := json.Marshal(paymentDetailSplit)
	// fmt.Println(string(bbbb))

	if flow.Payment.Payment.Split != nil {
		for _, incomingPaymentSplit := range flow.Payment.Payment.Split {
			if incomingPaymentSplit.Amount == nil {
				for index, paymentSplit := range paymentDetailSplit {
					if incomingPaymentSplit.Code == paymentSplit.Code {
						paymentDetailSplit = append(paymentDetailSplit[:index], paymentDetailSplit[index+1:]...)
					}
				}
			} else {
				for _, paymentSplit := range paymentDetailSplit {
					if incomingPaymentSplit.Code == paymentSplit.Code {
						paymentSplit.Amount = incomingPaymentSplit.Amount
					}
				}
			}
		}
	}

	// fmt.Println("CCCC")
	// cccc, _ := json.Marshal(paymentDetailSplit)
	// fmt.Println(string(cccc))
	// fmt.Println("DDDD")
	// dddd, _ := json.Marshal(flow.Payment.Payment.Split)
	// fmt.Println(string(dddd))

	flow.Payment.Payment.Split = paymentDetailSplit

	var totalAmount float64 = 0.0
	for _, paymentSplit := range flow.Payment.Payment.Split {
		totalAmount += *paymentSplit.Amount
	}

	// fmt.Println("EEEE")
	// eeee, _ := json.Marshal(flow.Payment.Payment.Split)
	// fmt.Println(string(eeee))

	if len(flow.Payment.Payment.Split) > 0 && flow.Payment.Payment.Amount != totalAmount {
		flow.Msg = "total amount and split amounts sum mismatching"

		MetricsPaymentsValidationError.Inc()

		return false
	}

	return true
}

func (flow *FlowProviderCreatePaymentDue) prepareRequest() bool {
	noticeCode, err := GenerateNoticeCode(flow.Sctx, flow.Tenant.IUVAux, flow.Tenant.IUVApp, flow.Tenant.IUVConsumer)
	if err != nil {
		flow.Err = err
		flow.Msg = "notice code generation error while preparing due request"
		return false
	}

	iuvCode := GetIuvFromNoticeCode(noticeCode)

	authenticationHash := apkappa.GetAuthenticationHash(iuvCode + flow.Tenant.Key)

	referenceYear, err := strconv.Atoi(flow.Payment.CreatedAt.Format("2006"))
	if err != nil {
		flow.Err = err
		flow.Msg = "reference year conversion error while preparing due request"
		return false
	}

	var payerType string
	if flow.Payment.Payer.Type == "human" {
		payerType = "F"
	} else {
		payerType = "G"
	}

	var address string
	address += flow.Payment.Payer.StreetName + ", " + flow.Payment.Payer.BuildingNumber
	address += " - " + flow.Payment.Payer.PostalCode + " " + flow.Payment.Payer.TownName
	address += " (" + flow.Payment.Payer.CountrySubdivision + ") "
	address += flow.Payment.Payer.Country

	listaDatiContabili := &apkappa.ArrayOfDatiContabili{
		DatiContabili: []*apkappa.DatiContabili{},
	}

	if flow.Payment.Payment.Split != nil {
		for _, paymentSplit := range flow.Payment.Payment.Split {

			datiContabili := &apkappa.DatiContabili{
				Tipologia:     paymentSplit.Meta.Type,
				ChiaveUnivoca: paymentSplit.Meta.Key,
				Quota:         *paymentSplit.Amount,
			}
			listaDatiContabili.DatiContabili = append(listaDatiContabili.DatiContabili, datiContabili)
		}
	}

	// fmt.Println("FFFF")
	// ffff, _ := json.Marshal(listaDatiContabili)
	// fmt.Println(string(ffff))

	flow.Request = &apkappa.CaricaListaVersamenti{
		RequestData: &apkappa.RequestCaricamentoListaVersamenti{
			Authentication: &apkappa.Authentication{
				Ente:           flow.Tenant.Code,
				CodiceServizio: flow.Service.Code,
				CodiceConsumer: flow.Tenant.Consumer,
				Timestamp:      soap.CreateXsdDateTime(time.Now(), false),
				Hash:           authenticationHash,
			},
			ListaDebiti: &apkappa.ArrayOfDebito{
				Debito: []*apkappa.Debito{
					{
						Id:                              iuvCode,
						Tipologia:                       flow.Service.Name,
						DescrizioneCausale:              flow.Service.Description,
						OggettoDelPagamento:             flow.Payment.Reason,
						DataCreazione:                   soap.CreateXsdDateTime(flow.Payment.CreatedAt.Time, false),
						DataEmissione:                   soap.CreateXsdDateTime(flow.Payment.CreatedAt.Time, false),
						DataPrescrizione:                soap.CreateXsdDateTime(flow.Payment.Payment.ExpireAt.Time, false),
						AnnoRiferimento:                 int16(referenceYear),
						ImportoTotale:                   flow.Payment.Payment.Amount,
						DebitoreCodice:                  flow.Payment.Payer.TaxIdentificationNumber,
						DebitorePersonaGF:               payerType,
						DebitoreCodiceFiscalePartitaIva: flow.Payment.Payer.TaxIdentificationNumber,
						DebitoreCognome:                 flow.Payment.Payer.FamilyName,
						DebitoreNome:                    flow.Payment.Payer.Name,
						DebitoreEMail:                   flow.Payment.Payer.Email,
						DebitoreIndirizzo:               address,
						DebitoreIndirizzo2:              "",
						CreditoreCodice:                 flow.Payment.Payer.TaxIdentificationNumber,
						CreditoreDescrizione:            flow.Payment.Payer.Name + " " + flow.Payment.Payer.FamilyName,
						Annotazioni:                     flow.Payment.Payment.IUD,
						// AvvisoNumeroGiorniDaDataNotifica: 1,
						ListaVersamenti: &apkappa.ArrayOfVersamento{
							Versamento: []*apkappa.Versamento{
								{
									Id:                 iuvCode,
									CodiceVersamento:   "",
									CodiceAvviso:       noticeCode,
									DescrizioneCausale: flow.Service.Description,
									DataScadenza:       soap.CreateXsdDateTime(flow.Payment.Payment.ExpireAt.Time, false),
									DataInizioValidita: soap.CreateXsdDateTime(flow.Payment.CreatedAt.Time, false),
									DataFineValidita:   soap.CreateXsdDateTime(flow.Payment.Payment.ExpireAt.Time, false),
									Importo:            flow.Payment.Payment.Amount,
									RataUnica:          true,
									Annotazioni:        flow.Payment.Payment.IUD,
									IdTabVers:          "",
									ImportiDettagliati: nil,
									// ImportiDettagliati: &apkappa.ArrayOfDettaglioImporti{
									// 	DettaglioImporti: []*apkappa.DettaglioImporti{
									// 		{
									// 			Tipologia:   apkappa.TipoImportiBase,
									// 			Descrizione: "",
									// 			Importo:     123.45,
									// 		},
									// 	},
									// },
									ListaDatiContabili: listaDatiContabili,
									// SpeseNotificaDaAttualizzare: false,
									// ImportoSpeseNotifica:        0.0,
								},
							},
						},
						// Metadati: &apkappa.ArrayOfDebitoMetadati{
						// 	DebitoMetadati: []*apkappa.DebitoMetadati{
						// 		{
						// 			NomeCampo:        "",
						// 			Dato:             "",
						// 			DescrizioneCampo: "",
						// 		},
						// 	},
						// },
					},
				},
			},
		},
	}

	flow.IUV = iuvCode

	return true
}

func (flow *FlowProviderCreatePaymentDue) doRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("create_payment_due"))

	flow.Client = soap.NewClient(flow.Tenant.Endpoint)

	flow.Apkappa = apkappa.NewService(flow.Client)

	var err error
	flow.Result, err = flow.Apkappa.CaricaListaVersamenti(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting due request"

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}

	requestResult := *flow.Result.CaricaListaVersamentiResult.Esito == apkappa.TipoEsitoOK
	if !requestResult {
		flow.Msg = "provider response ko"

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}

	paymentResult := false
	if flow.Result.CaricaListaVersamentiResult.ListaEsitiVersamenti != nil {
		for _, esitoVersamento := range flow.Result.CaricaListaVersamentiResult.ListaEsitiVersamenti.EsitoVersamento {
			paymentResult = *esitoVersamento.Esito == apkappa.TipoEsitoOK
		}
	}

	if !paymentResult {
		flow.Msg = "payment_due creation failed by provider ko"

		MetricsPaymentsProviderError.WithLabelValues("create_payment_due").Inc()

		return false
	}

	timer.ObserveDuration()

	return true
}
