package server

import (
	"time"

	"github.com/hooklift/gowsdl/soap"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/opencontent/stanza-del-cittadino/apkappa-payment-proxy/server/models"
	"gitlab.com/opencontent/stanza-del-cittadino/apkappa-payment-proxy/server/models/apkappa"
)

type FlowProviderVerifyPayment struct {
	Name      string
	Sctx      *ServerContext
	Payment   *models.Payment
	Service   *models.Service
	Tenant    *models.Tenant
	Request   *apkappa.VerificaStatoVersamenti
	Client    *soap.Client
	Apkappa   apkappa.Service
	Err       error
	Msg       string
	Status    bool
	Result    *apkappa.VerificaStatoVersamentiResponse
	Completed bool
	Payed     bool
	PaidAt    models.Time
	Receipt   []byte
}

func (flow *FlowProviderVerifyPayment) Exec() bool {
	flow.Name = "ProviderVerifyPayment"

	flow.Status = true &&
		flow.prepareRequest() &&
		flow.doRequest()

	return flow.Status
}

func (flow *FlowProviderVerifyPayment) prepareRequest() bool {
	iuvCode := flow.Payment.Payment.IUV

	authenticationHash := apkappa.GetAuthenticationHash(iuvCode + flow.Tenant.Key)

	flow.Request = &apkappa.VerificaStatoVersamenti{
		RequestData: &apkappa.RequestVerificaStatoVersamenti{
			Authentication: &apkappa.Authentication{
				Ente:           flow.Tenant.Code,
				CodiceServizio: flow.Service.Code,
				CodiceConsumer: flow.Tenant.Consumer,
				Timestamp:      soap.CreateXsdDateTime(time.Now(), false),
				Hash:           authenticationHash,
			},
			ListaVerifiche: &apkappa.ArrayOfVerificaVersamento{
				VerificaVersamento: []*apkappa.VerificaVersamento{
					{
						IdVersamento: iuvCode,
					},
				},
			},
		},
	}

	return true
}

func (flow *FlowProviderVerifyPayment) doRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("payment_verify"))

	flow.Client = soap.NewClient(flow.Tenant.Endpoint)

	flow.Apkappa = apkappa.NewService(flow.Client)

	var err error
	flow.Result, err = flow.Apkappa.VerificaStatoVersamenti(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting verify request"

		MetricsPaymentsProviderError.WithLabelValues("payment_verify").Inc()

		return false
	}

	requestResult := *flow.Result.VerificaStatoVersamentiResult.Esito == apkappa.TipoEsitoOK
	if !requestResult {
		flow.Msg = "provider response ko"

		MetricsPaymentsProviderError.WithLabelValues("payment_verify").Inc()

		return false
	}

	var paymentState apkappa.TipoEsitoPagamento
	var paymentTime models.Time
	if flow.Result.VerificaStatoVersamentiResult.ListaStatoVersamenti != nil {
		for _, statoVersamento := range flow.Result.VerificaStatoVersamentiResult.ListaStatoVersamenti.StatoVersamento {
			if statoVersamento.NotificaPagamento != nil {
				paymentState = *statoVersamento.NotificaPagamento.Esito
				payedAt := statoVersamento.NotificaPagamento.DataTransazione.ToGoTime()
				paymentTime = models.Time{Time: payedAt}
			} else {
				paymentState = apkappa.TipoEsitoPagamentoNonEseguito
			}
		}
	}

	timer.ObserveDuration()

	flow.Completed = paymentState == apkappa.TipoEsitoPagamentoEseguito || paymentState == apkappa.TipoEsitoPagamentoScaduto
	flow.Payed = paymentState == apkappa.TipoEsitoPagamentoEseguito
	flow.PaidAt = paymentTime

	return true
}
