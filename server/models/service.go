package models

type Service struct {
	ID          string         `json:"id"`
	TenantID    string         `json:"tenant_id"`
	Active      bool           `json:"active"`
	Code        string         `json:"code"`
	Name        string         `json:"name"`
	Description string         `json:"description"`
	Splitted    bool           `json:"splitted"`
	Split       []ServiceSplit `json:"split"`
}

type ServiceSplit struct {
	Code   string  `json:"split_code"`
	Amount float64 `json:"split_amount" description:"..." format:"float"`
	Type   string  `json:"split_type"`
	Key    string  `json:"split_key"`
}
