package apkappa

import (
	"crypto/sha1"
	"encoding/base64"
)

func GetAuthenticationHash(data string) string {
	hasher := sha1.New()
	hasher.Write([]byte(data))
	encodedHash := base64.StdEncoding.EncodeToString(hasher.Sum(nil))
	return encodedHash
}
