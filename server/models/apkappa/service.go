package apkappa

import (
	"context"
	"encoding/xml"
	"time"

	"github.com/hooklift/gowsdl/soap"
)

// against "unused imports"
var _ time.Time
var _ xml.Name

type AnyType struct {
	InnerXML string `xml:",innerxml"`
}

type AnyURI string

type NCName string

type TipoImporti string

const (
	TipoImportiSconosciuto TipoImporti = "Sconosciuto"

	TipoImportiBase TipoImporti = "Base"

	TipoImportiAggiuntivo TipoImporti = "Aggiuntivo"

	TipoImportiMultibeneficiario TipoImporti = "Multibeneficiario"
)

type TipoEsito string

const (
	TipoEsitoOK TipoEsito = "OK"

	TipoEsitoKO TipoEsito = "KO"

	TipoEsitoSconosciuto TipoEsito = "Sconosciuto"
)

type TipoStatoVersamento string

const (
	TipoStatoVersamentoNonPagabile TipoStatoVersamento = "NonPagabile"

	TipoStatoVersamentoNonPagato TipoStatoVersamento = "NonPagato"

	TipoStatoVersamentoPagato TipoStatoVersamento = "Pagato"

	TipoStatoVersamentoPagamentoIrregolare TipoStatoVersamento = "PagamentoIrregolare"

	TipoStatoVersamentoAnnullato TipoStatoVersamento = "Annullato"

	TipoStatoVersamentoPagamentoInCorso TipoStatoVersamento = "PagamentoInCorso"

	TipoStatoVersamentoScaduto TipoStatoVersamento = "Scaduto"

	TipoStatoVersamentoSconosciuto TipoStatoVersamento = "Sconosciuto"
)

type TipoPagamento string

const (
	TipoPagamentoPagamentoUnico TipoPagamento = "PagamentoUnico"

	TipoPagamentoPagamentoRate TipoPagamento = "PagamentoRate"

	TipoPagamentoSconosciuto TipoPagamento = "Sconosciuto"
)

type TipoEsitoPagamento string

const (
	TipoEsitoPagamentoEseguito TipoEsitoPagamento = "Eseguito"

	TipoEsitoPagamentoRegolato TipoEsitoPagamento = "Regolato"

	TipoEsitoPagamentoIncasso TipoEsitoPagamento = "Incasso"

	TipoEsitoPagamentoRevocato TipoEsitoPagamento = "Revocato"

	TipoEsitoPagamentoEseguitoSenzaRPT TipoEsitoPagamento = "EseguitoSenzaRPT"

	TipoEsitoPagamentoAvviato TipoEsitoPagamento = "Avviato"

	TipoEsitoPagamentoInErrore TipoEsitoPagamento = "InErrore"

	TipoEsitoPagamentoScaduto TipoEsitoPagamento = "Scaduto"

	TipoEsitoPagamentoNonEseguito TipoEsitoPagamento = "NonEseguito"

	TipoEsitoPagamentoSconosciuto TipoEsitoPagamento = "Sconosciuto"
)

type TipoDebito string

const (
	TipoDebitoPendenza TipoDebito = "Pendenza"

	TipoDebitoSpontaneo TipoDebito = "Spontaneo"

	TipoDebitoOTF TipoDebito = "OTF"

	TipoDebitoSconosciuto TipoDebito = "Sconosciuto"
)

type TipologiaDateLista string

const (
	TipologiaDateListaNonDefinito TipologiaDateLista = "NonDefinito"

	TipologiaDateListaDataRegolamento TipologiaDateLista = "DataRegolamento"

	TipologiaDateListaDataInserimento TipologiaDateLista = "DataInserimento"
)

type TipoRecuperoDati string

const (
	TipoRecuperoDatiNonRecuperato TipoRecuperoDati = "NonRecuperato"

	TipoRecuperoDatiCompleto TipoRecuperoDati = "Completo"

	TipoRecuperoDatiParziale TipoRecuperoDati = "Parziale"
)

type TipoAuxDigit string

const (
	TipoAuxDigitDigit0 TipoAuxDigit = "Digit0"

	TipoAuxDigitDigit3 TipoAuxDigit = "Digit3"
)

type TipoAutorizzazionePoste string

const (
	TipoAutorizzazionePosteSconosciuto TipoAutorizzazionePoste = "Sconosciuto"

	TipoAutorizzazionePostePDF TipoAutorizzazionePoste = "PDF"

	TipoAutorizzazionePosteCartaceo TipoAutorizzazionePoste = "Cartaceo"
)

type RicevutaTelematicaType string

const (
	RicevutaTelematicaTypeSconosciuto RicevutaTelematicaType = "Sconosciuto"

	RicevutaTelematicaTypeXml RicevutaTelematicaType = "Xml"

	RicevutaTelematicaTypePdf RicevutaTelematicaType = "Pdf"

	RicevutaTelematicaTypeReceipt RicevutaTelematicaType = "Receipt"
)

type TipoRichiestaEstrattoConto string

const (
	TipoRichiestaEstrattoContoI TipoRichiestaEstrattoConto = "I"
)

type CaricaListaVersamenti struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ CaricaListaVersamenti"`

	RequestData *RequestCaricamentoListaVersamenti `xml:"requestData,omitempty" json:"requestData,omitempty"`
}

type CaricaListaVersamentiResponse struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ CaricaListaVersamentiResponse"`

	CaricaListaVersamentiResult *ResponseCaricamentoListaVersamenti `xml:"CaricaListaVersamentiResult,omitempty" json:"CaricaListaVersamentiResult,omitempty"`
}

type VerificaStatoVersamenti struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ VerificaStatoVersamenti"`

	RequestData *RequestVerificaStatoVersamenti `xml:"requestData,omitempty" json:"requestData,omitempty"`
}

type VerificaStatoVersamentiResponse struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ VerificaStatoVersamentiResponse"`

	VerificaStatoVersamentiResult *ResponseVerificaStatoVersamenti `xml:"VerificaStatoVersamentiResult,omitempty" json:"VerificaStatoVersamentiResult,omitempty"`
}

type CaricamentoVersamentiOTF struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ CaricamentoVersamentiOTF"`

	RequestData *RequestCaricamentoVersamentiOTF `xml:"requestData,omitempty" json:"requestData,omitempty"`
}

type CaricamentoVersamentiOTFResponse struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ CaricamentoVersamentiOTFResponse"`

	CaricamentoVersamentiOTFResult *ResponseCaricamentoVersamentiOTF `xml:"CaricamentoVersamentiOTFResult,omitempty" json:"CaricamentoVersamentiOTFResult,omitempty"`
}

type VerificaTransazioneOTF struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ VerificaTransazioneOTF"`

	RequestData *RequestVerificaTransazioneOTF `xml:"requestData,omitempty" json:"requestData,omitempty"`
}

type VerificaTransazioneOTFResponse struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ VerificaTransazioneOTFResponse"`

	VerificaTransazioneOTFResult *ResponseVerificaTransazioneOTF `xml:"VerificaTransazioneOTFResult,omitempty" json:"VerificaTransazioneOTFResult,omitempty"`
}

type AnnullaListaVersamenti struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ AnnullaListaVersamenti"`

	RequestData *RequestCaricamentoListaVersamenti `xml:"requestData,omitempty" json:"requestData,omitempty"`
}

type AnnullaListaVersamentiResponse struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ AnnullaListaVersamentiResponse"`

	AnnullaListaVersamentiResult *ResponseCaricamentoListaVersamenti `xml:"AnnullaListaVersamentiResult,omitempty" json:"AnnullaListaVersamentiResult,omitempty"`
}

type NotificaPagamentoSenzaRPT struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ NotificaPagamentoSenzaRPT"`

	RequestData *RequestNotificaPagamentoSenzaRPT `xml:"requestData,omitempty" json:"requestData,omitempty"`
}

type NotificaPagamentoSenzaRPTResponse struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ NotificaPagamentoSenzaRPTResponse"`

	NotificaPagamentoSenzaRPTResult *ResponseNotificaPagamentoSenzaRPT `xml:"NotificaPagamentoSenzaRPTResult,omitempty" json:"NotificaPagamentoSenzaRPTResult,omitempty"`
}

type ChiediListaFlussiRendicontazione struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ ChiediListaFlussiRendicontazione"`

	RequestData *RequestChiediListaFlussiRendicontazione `xml:"requestData,omitempty" json:"requestData,omitempty"`
}

type ChiediListaFlussiRendicontazioneResponse struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ ChiediListaFlussiRendicontazioneResponse"`

	ChiediListaFlussiRendicontazioneResult *ResponseChiediListaFlussiRendicontazione `xml:"ChiediListaFlussiRendicontazioneResult,omitempty" json:"ChiediListaFlussiRendicontazioneResult,omitempty"`
}

type ChiediFlussiRendicontazione struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ ChiediFlussiRendicontazione"`

	RequestData *RequestChiediFlussiRendicontazione `xml:"requestData,omitempty" json:"requestData,omitempty"`
}

type ChiediFlussiRendicontazioneResponse struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ ChiediFlussiRendicontazioneResponse"`

	ChiediFlussiRendicontazioneResult *ResponseChiediFlussiRendicontazione `xml:"ChiediFlussiRendicontazioneResult,omitempty" json:"ChiediFlussiRendicontazioneResult,omitempty"`
}

type EstraiVersamentiAnonimi struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ EstraiVersamentiAnonimi"`

	RequestData *RequestEstraiVersamentiAnonimi `xml:"requestData,omitempty" json:"requestData,omitempty"`
}

type EstraiVersamentiAnonimiResponse struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ EstraiVersamentiAnonimiResponse"`

	EstraiVersamentiAnonimiResult *ResponseEstraiVersamentiAnonimi `xml:"EstraiVersamentiAnonimiResult,omitempty" json:"EstraiVersamentiAnonimiResult,omitempty"`
}

type OttieniParametriIuv struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ OttieniParametriIuv"`

	RequestData *RequestParametriIuv `xml:"requestData,omitempty" json:"requestData,omitempty"`
}

type OttieniParametriIuvResponse struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ OttieniParametriIuvResponse"`

	OttieniParametriIuvResult *ResponseParametriIuv `xml:"OttieniParametriIuvResult,omitempty" json:"OttieniParametriIuvResult,omitempty"`
}

type AggiornaListaVersamenti struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ AggiornaListaVersamenti"`

	RequestData *RequestCaricamentoListaVersamenti `xml:"requestData,omitempty" json:"requestData,omitempty"`
}

type AggiornaListaVersamentiResponse struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ AggiornaListaVersamentiResponse"`

	AggiornaListaVersamentiResult *ResponseCaricamentoListaVersamenti `xml:"AggiornaListaVersamentiResult,omitempty" json:"AggiornaListaVersamentiResult,omitempty"`
}

type DettaglioDebito struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ DettaglioDebito"`

	RequestData *RequestDettaglioDebito `xml:"requestData,omitempty" json:"requestData,omitempty"`
}

type DettaglioDebitoResponse struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ DettaglioDebitoResponse"`

	DettaglioDebitoResult *ResponseDettaglioDebito `xml:"DettaglioDebitoResult,omitempty" json:"DettaglioDebitoResult,omitempty"`
}

type ChiediAvvisiPagamento struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ ChiediAvvisiPagamento"`

	RequestData *RequestChiediAvvisiPagamento `xml:"requestData,omitempty" json:"requestData,omitempty"`
}

type ChiediAvvisiPagamentoResponse struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ ChiediAvvisiPagamentoResponse"`

	ChiediAvvisiPagamentoResult *ResponseChiediAvvisiPagamento `xml:"ChiediAvvisiPagamentoResult,omitempty" json:"ChiediAvvisiPagamentoResult,omitempty"`
}

type ChiediRicevutaTelematica struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ ChiediRicevutaTelematica"`

	RequestData *RequestChiediRicevutaTelematica `xml:"requestData,omitempty" json:"requestData,omitempty"`
}

type ChiediRicevutaTelematicaResponse struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ ChiediRicevutaTelematicaResponse"`

	ChiediRicevutaTelematicaResult *ResponseChiediRicevutaTelematica `xml:"ChiediRicevutaTelematicaResult,omitempty" json:"ChiediRicevutaTelematicaResult,omitempty"`
}

type ChiediEstrattoConto struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ ChiediEstrattoConto"`

	RequestData *RequestChiediEstrattoConto `xml:"requestData,omitempty" json:"requestData,omitempty"`
}

type ChiediEstrattoContoResponse struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ ChiediEstrattoContoResponse"`

	ChiediEstrattoContoResult *ResponseChiediEstrattoConto `xml:"ChiediEstrattoContoResult,omitempty" json:"ChiediEstrattoContoResult,omitempty"`
}

type ChiediVersamentiPagati struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ ChiediVersamentiPagati"`

	RequestData *RequestChiediVersamentiPagati `xml:"requestData,omitempty" json:"requestData,omitempty"`
}

type ChiediVersamentiPagatiResponse struct {
	XMLName xml.Name `xml:"http://services.apsystems.it/ ChiediVersamentiPagatiResponse"`

	ChiediVersamentiPagatiResult *ResponseChiediVersamentiPagati `xml:"ChiediVersamentiPagatiResult,omitempty" json:"ChiediVersamentiPagatiResult,omitempty"`
}

type RequestCaricamentoListaVersamenti struct {
	Authentication *Authentication `xml:"Authentication,omitempty" json:"Authentication,omitempty"`

	ListaDebiti *ArrayOfDebito `xml:"ListaDebiti,omitempty" json:"ListaDebiti,omitempty"`
}

type Authentication struct {
	Ente string `xml:"Ente,omitempty" json:"Ente,omitempty"`

	CodiceServizio string `xml:"CodiceServizio,omitempty" json:"CodiceServizio,omitempty"`

	CodiceConsumer string `xml:"CodiceConsumer,omitempty" json:"CodiceConsumer,omitempty"`

	Timestamp soap.XSDDateTime `xml:"Timestamp,omitempty" json:"Timestamp,omitempty"`

	Hash string `xml:"Hash,omitempty" json:"Hash,omitempty"`
}

type ArrayOfDebito struct {
	Debito []*Debito `xml:"Debito,omitempty" json:"Debito,omitempty"`
}

type Debito struct {
	Id string `xml:"Id,omitempty" json:"Id,omitempty"`

	Tipologia string `xml:"Tipologia,omitempty" json:"Tipologia,omitempty"`

	DescrizioneCausale string `xml:"DescrizioneCausale,omitempty" json:"DescrizioneCausale,omitempty"`

	OggettoDelPagamento string `xml:"OggettoDelPagamento,omitempty" json:"OggettoDelPagamento,omitempty"`

	DataCreazione soap.XSDDateTime `xml:"DataCreazione,omitempty" json:"DataCreazione,omitempty"`

	DataEmissione soap.XSDDateTime `xml:"DataEmissione,omitempty" json:"DataEmissione,omitempty"`

	DataPrescrizione soap.XSDDateTime `xml:"DataPrescrizione,omitempty" json:"DataPrescrizione,omitempty"`

	AnnoRiferimento int16 `xml:"AnnoRiferimento,omitempty" json:"AnnoRiferimento,omitempty"`

	ImportoTotale float64 `xml:"ImportoTotale,omitempty" json:"ImportoTotale,omitempty"`

	DebitoreCodice string `xml:"DebitoreCodice,omitempty" json:"DebitoreCodice,omitempty"`

	DebitorePersonaGF string `xml:"DebitorePersonaGF,omitempty" json:"DebitorePersonaGF,omitempty"`

	DebitoreCodiceFiscalePartitaIva string `xml:"DebitoreCodiceFiscalePartitaIva,omitempty" json:"DebitoreCodiceFiscalePartitaIva,omitempty"`

	DebitoreCognome string `xml:"DebitoreCognome,omitempty" json:"DebitoreCognome,omitempty"`

	DebitoreNome string `xml:"DebitoreNome,omitempty" json:"DebitoreNome,omitempty"`

	DebitoreEMail string `xml:"DebitoreEMail,omitempty" json:"DebitoreEMail,omitempty"`

	DebitoreIndirizzo string `xml:"DebitoreIndirizzo,omitempty" json:"DebitoreIndirizzo,omitempty"`

	DebitoreIndirizzo2 string `xml:"DebitoreIndirizzo2,omitempty" json:"DebitoreIndirizzo2,omitempty"`

	CreditoreCodice string `xml:"CreditoreCodice,omitempty" json:"CreditoreCodice,omitempty"`

	CreditoreDescrizione string `xml:"CreditoreDescrizione,omitempty" json:"CreditoreDescrizione,omitempty"`

	Annotazioni string `xml:"Annotazioni,omitempty" json:"Annotazioni,omitempty"`

	AvvisoNumeroGiorniDaDataNotifica int32 `xml:"AvvisoNumeroGiorniDaDataNotifica,omitempty" json:"AvvisoNumeroGiorniDaDataNotifica,omitempty"`

	ListaVersamenti *ArrayOfVersamento `xml:"ListaVersamenti,omitempty" json:"ListaVersamenti,omitempty"`

	Metadati *ArrayOfDebitoMetadati `xml:"Metadati,omitempty" json:"Metadati,omitempty"`
}

type ArrayOfVersamento struct {
	Versamento []*Versamento `xml:"Versamento,omitempty" json:"Versamento,omitempty"`
}

type Versamento struct {
	Id string `xml:"Id,omitempty" json:"Id,omitempty"`

	CodiceVersamento string `xml:"CodiceVersamento,omitempty" json:"CodiceVersamento,omitempty"`

	CodiceAvviso string `xml:"CodiceAvviso,omitempty" json:"CodiceAvviso,omitempty"`

	DescrizioneCausale string `xml:"DescrizioneCausale,omitempty" json:"DescrizioneCausale,omitempty"`

	DataScadenza soap.XSDDateTime `xml:"DataScadenza,omitempty" json:"DataScadenza,omitempty"`

	DataInizioValidita soap.XSDDateTime `xml:"DataInizioValidita,omitempty" json:"DataInizioValidita,omitempty"`

	DataFineValidita soap.XSDDateTime `xml:"DataFineValidita,omitempty" json:"DataFineValidita,omitempty"`

	Importo float64 `xml:"Importo,omitempty" json:"Importo,omitempty"`

	RataUnica bool `xml:"RataUnica,omitempty" json:"RataUnica,omitempty"`

	Annotazioni string `xml:"Annotazioni,omitempty" json:"Annotazioni,omitempty"`

	IdTabVers string `xml:"IdTabVers,omitempty" json:"IdTabVers,omitempty"`

	ImportiDettagliati *ArrayOfDettaglioImporti `xml:"ImportiDettagliati,omitempty" json:"ImportiDettagliati,omitempty"`

	ListaDatiContabili *ArrayOfDatiContabili `xml:"ListaDatiContabili,omitempty" json:"ListaDatiContabili,omitempty"`

	SpeseNotificaDaAttualizzare bool `xml:"SpeseNotificaDaAttualizzare,omitempty" json:"SpeseNotificaDaAttualizzare,omitempty"`

	ImportoSpeseNotifica float64 `xml:"ImportoSpeseNotifica,omitempty" json:"ImportoSpeseNotifica,omitempty"`
}

type ArrayOfDettaglioImporti struct {
	DettaglioImporti []*DettaglioImporti `xml:"DettaglioImporti,omitempty" json:"DettaglioImporti,omitempty"`
}

type DettaglioImporti struct {
	Tipologia *TipoImporti `xml:"Tipologia,omitempty" json:"Tipologia,omitempty"`

	Descrizione string `xml:"Descrizione,omitempty" json:"Descrizione,omitempty"`

	Importo float64 `xml:"Importo,omitempty" json:"Importo,omitempty"`
}

type ArrayOfDatiContabili struct {
	DatiContabili []*DatiContabili `xml:"DatiContabili,omitempty" json:"DatiContabili,omitempty"`
}

type DatiContabili struct {
	Tipologia string `xml:"Tipologia,omitempty" json:"Tipologia,omitempty"`

	ChiaveUnivoca string `xml:"ChiaveUnivoca,omitempty" json:"ChiaveUnivoca,omitempty"`

	Quota float64 `xml:"Quota,omitempty" json:"Quota,omitempty"`
}

type ArrayOfDebitoMetadati struct {
	DebitoMetadati []*DebitoMetadati `xml:"DebitoMetadati,omitempty" json:"DebitoMetadati,omitempty"`
}

type DebitoMetadati struct {
	NomeCampo string `xml:"NomeCampo,omitempty" json:"NomeCampo,omitempty"`

	Dato string `xml:"Dato,omitempty" json:"Dato,omitempty"`

	DescrizioneCampo string `xml:"DescrizioneCampo,omitempty" json:"DescrizioneCampo,omitempty"`
}

type ResponseCaricamentoListaVersamenti struct {
	Esito *TipoEsito `xml:"Esito,omitempty" json:"Esito,omitempty"`

	ListaDettagliEsiti *ArrayOfDettaglioEsito `xml:"ListaDettagliEsiti,omitempty" json:"ListaDettagliEsiti,omitempty"`

	Note string `xml:"Note,omitempty" json:"Note,omitempty"`

	IdTransazione string `xml:"IdTransazione,omitempty" json:"IdTransazione,omitempty"`

	ListaEsitiVersamenti *ArrayOfEsitoVersamento `xml:"ListaEsitiVersamenti,omitempty" json:"ListaEsitiVersamenti,omitempty"`
}

type ArrayOfDettaglioEsito struct {
	DettaglioEsito []*DettaglioEsito `xml:"DettaglioEsito,omitempty" json:"DettaglioEsito,omitempty"`
}

type DettaglioEsito struct {
	Codice string `xml:"Codice,omitempty" json:"Codice,omitempty"`

	Descrizione string `xml:"Descrizione,omitempty" json:"Descrizione,omitempty"`

	Elemento string `xml:"Elemento,omitempty" json:"Elemento,omitempty"`

	Note string `xml:"Note,omitempty" json:"Note,omitempty"`
}

type ArrayOfEsitoVersamento struct {
	EsitoVersamento []*EsitoVersamento `xml:"EsitoVersamento,omitempty" json:"EsitoVersamento,omitempty"`
}

type EsitoVersamento struct {
	Id string `xml:"Id,omitempty" json:"Id,omitempty"`

	Esito *TipoEsito `xml:"Esito,omitempty" json:"Esito,omitempty"`

	ListaDettagliEsiti *ArrayOfDettaglioEsito `xml:"ListaDettagliEsiti,omitempty" json:"ListaDettagliEsiti,omitempty"`

	Note string `xml:"Note,omitempty" json:"Note,omitempty"`
}

type RequestVerificaStatoVersamenti struct {
	Authentication *Authentication `xml:"Authentication,omitempty" json:"Authentication,omitempty"`

	ListaVerifiche *ArrayOfVerificaVersamento `xml:"ListaVerifiche,omitempty" json:"ListaVerifiche,omitempty"`
}

type ArrayOfVerificaVersamento struct {
	VerificaVersamento []*VerificaVersamento `xml:"VerificaVersamento,omitempty" json:"VerificaVersamento,omitempty"`
}

type VerificaVersamento struct {
	IdVersamento string `xml:"IdVersamento,omitempty" json:"IdVersamento,omitempty"`

	Tipologia string `xml:"Tipologia,omitempty" json:"Tipologia,omitempty"`
}

type ResponseVerificaStatoVersamenti struct {
	Esito *TipoEsito `xml:"Esito,omitempty" json:"Esito,omitempty"`

	DettagliEsito *ArrayOfDettaglioEsito `xml:"DettagliEsito,omitempty" json:"DettagliEsito,omitempty"`

	ListaStatoVersamenti *ArrayOfStatoVersamento `xml:"ListaStatoVersamenti,omitempty" json:"ListaStatoVersamenti,omitempty"`
}

type ArrayOfStatoVersamento struct {
	StatoVersamento []*StatoVersamento `xml:"StatoVersamento,omitempty" json:"StatoVersamento,omitempty"`
}

type StatoVersamento struct {
	IdVersamento string `xml:"IdVersamento,omitempty" json:"IdVersamento,omitempty"`

	Tipologia string `xml:"Tipologia,omitempty" json:"Tipologia,omitempty"`

	CodiceConsumer string `xml:"CodiceConsumer,omitempty" json:"CodiceConsumer,omitempty"`

	Stato *TipoStatoVersamento `xml:"Stato,omitempty" json:"Stato,omitempty"`

	DescrizioneStatoVersamento string `xml:"DescrizioneStatoVersamento,omitempty" json:"DescrizioneStatoVersamento,omitempty"`

	NoteStatoVersamento string `xml:"NoteStatoVersamento,omitempty" json:"NoteStatoVersamento,omitempty"`

	NotificaPagamento *NotificaPagamento `xml:"NotificaPagamento,omitempty" json:"NotificaPagamento,omitempty"`

	StoricoPagamenti *ArrayOfNotificaPagamento `xml:"StoricoPagamenti,omitempty" json:"StoricoPagamenti,omitempty"`
}

type NotificaPagamento struct {
	IdPagamento string `xml:"IdPagamento,omitempty" json:"IdPagamento,omitempty"`

	CCP string `xml:"CCP,omitempty" json:"CCP,omitempty"`

	Ente string `xml:"Ente,omitempty" json:"Ente,omitempty"`

	CodiceConsumer string `xml:"CodiceConsumer,omitempty" json:"CodiceConsumer,omitempty"`

	CodicePagamento string `xml:"CodicePagamento,omitempty" json:"CodicePagamento,omitempty"`

	TipoPagamento *TipoPagamento `xml:"TipoPagamento,omitempty" json:"TipoPagamento,omitempty"`

	DataPagamento soap.XSDDateTime `xml:"DataPagamento,omitempty" json:"DataPagamento,omitempty"`

	DataScadenzaPagamento soap.XSDDateTime `xml:"DataScadenzaPagamento,omitempty" json:"DataScadenzaPagamento,omitempty"`

	Importo float64 `xml:"Importo,omitempty" json:"Importo,omitempty"`

	Esito *TipoEsitoPagamento `xml:"Esito,omitempty" json:"Esito,omitempty"`

	RiferimentoDebitore string `xml:"RiferimentoDebitore,omitempty" json:"RiferimentoDebitore,omitempty"`

	IdPagante string `xml:"IdPagante,omitempty" json:"IdPagante,omitempty"`

	DescrizionePagante string `xml:"DescrizionePagante,omitempty" json:"DescrizionePagante,omitempty"`

	TipoCanalePagamento string `xml:"TipoCanalePagamento,omitempty" json:"TipoCanalePagamento,omitempty"`

	DescrizioneCanalePagamento string `xml:"DescrizioneCanalePagamento,omitempty" json:"DescrizioneCanalePagamento,omitempty"`

	PspCanalePagamento string `xml:"PspCanalePagamento,omitempty" json:"PspCanalePagamento,omitempty"`

	TipoMezzoPagamento string `xml:"TipoMezzoPagamento,omitempty" json:"TipoMezzoPagamento,omitempty"`

	DescrizioneMezzoPagamento string `xml:"DescrizioneMezzoPagamento,omitempty" json:"DescrizioneMezzoPagamento,omitempty"`

	FilialeCanalePagamento string `xml:"FilialeCanalePagamento,omitempty" json:"FilialeCanalePagamento,omitempty"`

	SportelloCanalePagamento string `xml:"SportelloCanalePagamento,omitempty" json:"SportelloCanalePagamento,omitempty"`

	IdTerminaleCanalePagamento string `xml:"IdTerminaleCanalePagamento,omitempty" json:"IdTerminaleCanalePagamento,omitempty"`

	IdOperazioneCanalePagamento string `xml:"IdOperazioneCanalePagamento,omitempty" json:"IdOperazioneCanalePagamento,omitempty"`

	IdTransazione string `xml:"IdTransazione,omitempty" json:"IdTransazione,omitempty"`

	DataTransazione soap.XSDDateTime `xml:"DataTransazione,omitempty" json:"DataTransazione,omitempty"`

	CodiceAutorizzazioneTransazione string `xml:"CodiceAutorizzazioneTransazione,omitempty" json:"CodiceAutorizzazioneTransazione,omitempty"`

	DataAutorizzazioneTransazione soap.XSDDateTime `xml:"DataAutorizzazioneTransazione,omitempty" json:"DataAutorizzazioneTransazione,omitempty"`

	TipoSicurezzaTransazione string `xml:"TipoSicurezzaTransazione,omitempty" json:"TipoSicurezzaTransazione,omitempty"`

	ImportoTransazione float64 `xml:"ImportoTransazione,omitempty" json:"ImportoTransazione,omitempty"`

	CausaleTransazione string `xml:"CausaleTransazione,omitempty" json:"CausaleTransazione,omitempty"`

	TipoDebito *TipoDebito `xml:"TipoDebito,omitempty" json:"TipoDebito,omitempty"`

	IdDebito string `xml:"IdDebito,omitempty" json:"IdDebito,omitempty"`

	QuietanzaCartacea bool `xml:"QuietanzaCartacea,omitempty" json:"QuietanzaCartacea,omitempty"`

	Note string `xml:"Note,omitempty" json:"Note,omitempty"`

	DescrizioneCausale string `xml:"DescrizioneCausale,omitempty" json:"DescrizioneCausale,omitempty"`

	IdentificativoUnivocoVersamento string `xml:"IdentificativoUnivocoVersamento,omitempty" json:"IdentificativoUnivocoVersamento,omitempty"`

	IdentificativoUnivocoRiscossione string `xml:"IdentificativoUnivocoRiscossione,omitempty" json:"IdentificativoUnivocoRiscossione,omitempty"`

	IdVersamento string `xml:"IdVersamento,omitempty" json:"IdVersamento,omitempty"`

	Beneficiario string `xml:"Beneficiario,omitempty" json:"Beneficiario,omitempty"`

	CodFiscaleBeneficiario string `xml:"CodFiscaleBeneficiario,omitempty" json:"CodFiscaleBeneficiario,omitempty"`

	XmlRT string `xml:"XmlRT,omitempty" json:"XmlRT,omitempty"`

	IdRichiestaRPT string `xml:"IdRichiestaRPT,omitempty" json:"IdRichiestaRPT,omitempty"`

	DataRichiestaRPT soap.XSDDateTime `xml:"DataRichiestaRPT,omitempty" json:"DataRichiestaRPT,omitempty"`

	IsRT bool `xml:"isRT,omitempty" json:"isRT,omitempty"`

	Tassonomia string `xml:"Tassonomia,omitempty" json:"Tassonomia,omitempty"`
}

type ArrayOfNotificaPagamento struct {
	NotificaPagamento []*NotificaPagamento `xml:"NotificaPagamento,omitempty" json:"NotificaPagamento,omitempty"`
}

type RequestCaricamentoVersamentiOTF struct {
	Authentication *Authentication `xml:"Authentication,omitempty" json:"Authentication,omitempty"`

	Url *URLRedirect `xml:"Url,omitempty" json:"Url,omitempty"`

	ListaDebiti *ArrayOfDebitoOTF `xml:"ListaDebiti,omitempty" json:"ListaDebiti,omitempty"`
}

type URLRedirect struct {
	URLDone string `xml:"URLDone,omitempty" json:"URLDone,omitempty"`

	URLError string `xml:"URLError,omitempty" json:"URLError,omitempty"`

	URLBack string `xml:"URLBack,omitempty" json:"URLBack,omitempty"`
}

type ArrayOfDebitoOTF struct {
	DebitoOTF []*DebitoOTF `xml:"DebitoOTF,omitempty" json:"DebitoOTF,omitempty"`
}

type DebitoOTF struct {
	DebitoreCodiceFiscalePartitaIva string `xml:"DebitoreCodiceFiscalePartitaIva,omitempty" json:"DebitoreCodiceFiscalePartitaIva,omitempty"`

	DebitoreCognome string `xml:"DebitoreCognome,omitempty" json:"DebitoreCognome,omitempty"`

	DebitoreNome string `xml:"DebitoreNome,omitempty" json:"DebitoreNome,omitempty"`

	DebitoreEMail string `xml:"DebitoreEMail,omitempty" json:"DebitoreEMail,omitempty"`

	DebitoreIndirizzo string `xml:"DebitoreIndirizzo,omitempty" json:"DebitoreIndirizzo,omitempty"`

	DebitoreIndirizzo2 string `xml:"DebitoreIndirizzo2,omitempty" json:"DebitoreIndirizzo2,omitempty"`

	Id string `xml:"Id,omitempty" json:"Id,omitempty"`

	Tipologia string `xml:"Tipologia,omitempty" json:"Tipologia,omitempty"`

	DescrizioneCausale string `xml:"DescrizioneCausale,omitempty" json:"DescrizioneCausale,omitempty"`

	OggettoDelPagamento string `xml:"OggettoDelPagamento,omitempty" json:"OggettoDelPagamento,omitempty"`

	Importo float64 `xml:"Importo,omitempty" json:"Importo,omitempty"`

	CodiceIUV string `xml:"CodiceIUV,omitempty" json:"CodiceIUV,omitempty"`

	CodiceAvviso string `xml:"CodiceAvviso,omitempty" json:"CodiceAvviso,omitempty"`

	DataScadenza soap.XSDDateTime `xml:"DataScadenza,omitempty" json:"DataScadenza,omitempty"`

	CodiceConsumer string `xml:"CodiceConsumer,omitempty" json:"CodiceConsumer,omitempty"`
}

type ResponseCaricamentoVersamentiOTF struct {
	Esito *TipoEsito `xml:"Esito,omitempty" json:"Esito,omitempty"`

	Note string `xml:"Note,omitempty" json:"Note,omitempty"`

	IdTransazione string `xml:"IdTransazione,omitempty" json:"IdTransazione,omitempty"`

	EsitoDebiti *ArrayOfEsitoDebito `xml:"EsitoDebiti,omitempty" json:"EsitoDebiti,omitempty"`

	Url string `xml:"Url,omitempty" json:"Url,omitempty"`
}

type ArrayOfEsitoDebito struct {
	EsitoDebito []*EsitoDebito `xml:"EsitoDebito,omitempty" json:"EsitoDebito,omitempty"`
}

type EsitoDebito struct {
	Id string `xml:"Id,omitempty" json:"Id,omitempty"`

	Esito *TipoEsito `xml:"Esito,omitempty" json:"Esito,omitempty"`

	ListaEsitiVersamenti *ArrayOfEsitoVersamento `xml:"ListaEsitiVersamenti,omitempty" json:"ListaEsitiVersamenti,omitempty"`

	Note string `xml:"Note,omitempty" json:"Note,omitempty"`
}

type RequestVerificaTransazioneOTF struct {
	Authentication *Authentication `xml:"Authentication,omitempty" json:"Authentication,omitempty"`

	IdTransazione string `xml:"IdTransazione,omitempty" json:"IdTransazione,omitempty"`
}

type ResponseVerificaTransazioneOTF struct {
	IdTransazione string `xml:"IdTransazione,omitempty" json:"IdTransazione,omitempty"`

	Esito *TipoEsito `xml:"Esito,omitempty" json:"Esito,omitempty"`

	NoteEsito string `xml:"NoteEsito,omitempty" json:"NoteEsito,omitempty"`

	IdTransazioneRichiesta string `xml:"IdTransazioneRichiesta,omitempty" json:"IdTransazioneRichiesta,omitempty"`

	StatoTransazioneRichiesta string `xml:"StatoTransazioneRichiesta,omitempty" json:"StatoTransazioneRichiesta,omitempty"`

	NoteTransazioneRichiesta string `xml:"NoteTransazioneRichiesta,omitempty" json:"NoteTransazioneRichiesta,omitempty"`

	ListaStatoVersamenti *ArrayOfStatoVersamento `xml:"ListaStatoVersamenti,omitempty" json:"ListaStatoVersamenti,omitempty"`
}

type RequestNotificaPagamentoSenzaRPT struct {
	Authentication *Authentication `xml:"Authentication,omitempty" json:"Authentication,omitempty"`

	ListaPagamenti *ArrayOfNotificaPagamento `xml:"ListaPagamenti,omitempty" json:"ListaPagamenti,omitempty"`
}

type ResponseNotificaPagamentoSenzaRPT struct {
	Esito *TipoEsito `xml:"Esito,omitempty" json:"Esito,omitempty"`

	ListaDettagliEsiti *ArrayOfDettaglioEsito `xml:"ListaDettagliEsiti,omitempty" json:"ListaDettagliEsiti,omitempty"`

	Note string `xml:"Note,omitempty" json:"Note,omitempty"`

	IdTransazione string `xml:"IdTransazione,omitempty" json:"IdTransazione,omitempty"`

	ListaEsitiNotifiche *ArrayOfEsitoNotificaPagamento `xml:"ListaEsitiNotifiche,omitempty" json:"ListaEsitiNotifiche,omitempty"`
}

type ArrayOfEsitoNotificaPagamento struct {
	EsitoNotificaPagamento []*EsitoNotificaPagamento `xml:"EsitoNotificaPagamento,omitempty" json:"EsitoNotificaPagamento,omitempty"`
}

type EsitoNotificaPagamento struct {
	Id string `xml:"Id,omitempty" json:"Id,omitempty"`

	Esito *TipoEsito `xml:"Esito,omitempty" json:"Esito,omitempty"`

	Codice string `xml:"Codice,omitempty" json:"Codice,omitempty"`

	Descrizione string `xml:"Descrizione,omitempty" json:"Descrizione,omitempty"`

	Elemento string `xml:"Elemento,omitempty" json:"Elemento,omitempty"`

	Note string `xml:"Note,omitempty" json:"Note,omitempty"`
}

type RequestChiediListaFlussiRendicontazione struct {
	Authentication *Authentication `xml:"Authentication,omitempty" json:"Authentication,omitempty"`

	TipologiaDate *TipologiaDateLista `xml:"TipologiaDate,omitempty" json:"TipologiaDate,omitempty"`

	DataInizio soap.XSDDateTime `xml:"DataInizio,omitempty" json:"DataInizio,omitempty"`

	DataFine soap.XSDDateTime `xml:"DataFine,omitempty" json:"DataFine,omitempty"`
}

type ResponseChiediListaFlussiRendicontazione struct {
	Esito *TipoEsito `xml:"Esito,omitempty" json:"Esito,omitempty"`

	Note string `xml:"Note,omitempty" json:"Note,omitempty"`

	IdTransazione string `xml:"IdTransazione,omitempty" json:"IdTransazione,omitempty"`

	ListaFlussiRendicontazione *ListaFlussiRendicontazione `xml:"ListaFlussiRendicontazione,omitempty" json:"ListaFlussiRendicontazione,omitempty"`
}

type ListaFlussiRendicontazione struct {
	EsitoRichiesta *EsitoRichiesta `xml:"EsitoRichiesta,omitempty" json:"EsitoRichiesta,omitempty"`

	FlussoRendicontazione *ArrayOfFlussoRendicontazione `xml:"FlussoRendicontazione,omitempty" json:"FlussoRendicontazione,omitempty"`
}

type EsitoRichiesta struct {
	Codice string `xml:"Codice,omitempty" json:"Codice,omitempty"`

	CodiceEsitoOperazione string `xml:"CodiceEsitoOperazione,omitempty" json:"CodiceEsitoOperazione,omitempty"`

	EsitoOperazioneDescr string `xml:"EsitoOperazioneDescr,omitempty" json:"EsitoOperazioneDescr,omitempty"`
}

type ArrayOfFlussoRendicontazione struct {
	FlussoRendicontazione []*FlussoRendicontazione `xml:"FlussoRendicontazione,omitempty" json:"FlussoRendicontazione,omitempty"`
}

type FlussoRendicontazione struct {
	VersioneOggetto string `xml:"VersioneOggetto,omitempty" json:"VersioneOggetto,omitempty"`

	CodiceFlusso string `xml:"CodiceFlusso,omitempty" json:"CodiceFlusso,omitempty"`

	IdentificativoFlusso string `xml:"IdentificativoFlusso,omitempty" json:"IdentificativoFlusso,omitempty"`

	DataFlusso soap.XSDDateTime `xml:"DataFlusso,omitempty" json:"DataFlusso,omitempty"`

	Iur string `xml:"Iur,omitempty" json:"Iur,omitempty"`

	DataRegolamento soap.XSDDateTime `xml:"DataRegolamento,omitempty" json:"DataRegolamento,omitempty"`

	TipoPsp string `xml:"TipoPsp,omitempty" json:"TipoPsp,omitempty"`

	CodicePsp string `xml:"CodicePsp,omitempty" json:"CodicePsp,omitempty"`

	DenominazionePsp string `xml:"DenominazionePsp,omitempty" json:"DenominazionePsp,omitempty"`

	CodiceBicRiversamento string `xml:"CodiceBicRiversamento,omitempty" json:"CodiceBicRiversamento,omitempty"`

	TipoRicevente string `xml:"TipoRicevente,omitempty" json:"TipoRicevente,omitempty"`

	CodiceRicevente string `xml:"CodiceRicevente,omitempty" json:"CodiceRicevente,omitempty"`

	DenominazioneRicevente string `xml:"DenominazioneRicevente,omitempty" json:"DenominazioneRicevente,omitempty"`

	NumeroPagamenti int32 `xml:"NumeroPagamenti,omitempty" json:"NumeroPagamenti,omitempty"`

	ImportoTotale float64 `xml:"ImportoTotale,omitempty" json:"ImportoTotale,omitempty"`

	Stato string `xml:"Stato,omitempty" json:"Stato,omitempty"`

	DescrizioneStato string `xml:"DescrizioneStato,omitempty" json:"DescrizioneStato,omitempty"`

	Anomalia string `xml:"Anomalia,omitempty" json:"Anomalia,omitempty"`

	Pagamenti *ArrayOfDatiPagamento `xml:"Pagamenti,omitempty" json:"Pagamenti,omitempty"`
}

type ArrayOfDatiPagamento struct {
	DatiPagamento []*DatiPagamento `xml:"DatiPagamento,omitempty" json:"DatiPagamento,omitempty"`
}

type DatiPagamento struct {
	CodiceVersamento string `xml:"CodiceVersamento,omitempty" json:"CodiceVersamento,omitempty"`

	Iuv string `xml:"Iuv,omitempty" json:"Iuv,omitempty"`

	IuvOriginale string `xml:"IuvOriginale,omitempty" json:"IuvOriginale,omitempty"`

	Iur string `xml:"Iur,omitempty" json:"Iur,omitempty"`

	IndiceProgressivoPag string `xml:"IndiceProgressivoPag,omitempty" json:"IndiceProgressivoPag,omitempty"`

	ImportoRendicontato float64 `xml:"ImportoRendicontato,omitempty" json:"ImportoRendicontato,omitempty"`

	CodiceEsitoRendicontazione string `xml:"CodiceEsitoRendicontazione,omitempty" json:"CodiceEsitoRendicontazione,omitempty"`

	DataRendicontazione soap.XSDDateTime `xml:"DataRendicontazione,omitempty" json:"DataRendicontazione,omitempty"`

	EsitoRendicontazione *TipoEsitoPagamento `xml:"EsitoRendicontazione,omitempty" json:"EsitoRendicontazione,omitempty"`

	CodApplicazione string `xml:"CodApplicazione,omitempty" json:"CodApplicazione,omitempty"`

	CodiceVersamentoEnte string `xml:"CodiceVersamentoEnte,omitempty" json:"CodiceVersamentoEnte,omitempty"`

	Causale string `xml:"Causale,omitempty" json:"Causale,omitempty"`

	CausaleRata string `xml:"CausaleRata,omitempty" json:"CausaleRata,omitempty"`

	Debitore *DatiIndividuo `xml:"Debitore,omitempty" json:"Debitore,omitempty"`

	Versante *DatiIndividuo `xml:"Versante,omitempty" json:"Versante,omitempty"`

	Stato string `xml:"Stato,omitempty" json:"Stato,omitempty"`

	Anomalia string `xml:"Anomalia,omitempty" json:"Anomalia,omitempty"`

	ListaDatiContabili *ArrayOfDatiContabili `xml:"ListaDatiContabili,omitempty" json:"ListaDatiContabili,omitempty"`

	ProgressivoIuv int32 `xml:"ProgressivoIuv,omitempty" json:"ProgressivoIuv,omitempty"`

	TipoRecuperoDati *TipoRecuperoDati `xml:"TipoRecuperoDati,omitempty" json:"TipoRecuperoDati,omitempty"`
}

type DatiIndividuo struct {
	Tipologia string `xml:"Tipologia,omitempty" json:"Tipologia,omitempty"`

	Identificativo string `xml:"Identificativo,omitempty" json:"Identificativo,omitempty"`

	Cognome string `xml:"Cognome,omitempty" json:"Cognome,omitempty"`

	Nome string `xml:"Nome,omitempty" json:"Nome,omitempty"`
}

type RequestChiediFlussiRendicontazione struct {
	Authentication *Authentication `xml:"Authentication,omitempty" json:"Authentication,omitempty"`

	CodiciFlusso *ArrayOfString `xml:"CodiciFlusso,omitempty" json:"CodiciFlusso,omitempty"`

	CodiciIUV *ArrayOfString `xml:"CodiciIUV,omitempty" json:"CodiciIUV,omitempty"`
}

type ArrayOfString struct {
	Astring []*string `xml:"string,omitempty" json:"string,omitempty"`
}

type ResponseChiediFlussiRendicontazione struct {
	Esito *TipoEsito `xml:"Esito,omitempty" json:"Esito,omitempty"`

	ListaDettagliEsiti *ArrayOfDettaglioEsito `xml:"ListaDettagliEsiti,omitempty" json:"ListaDettagliEsiti,omitempty"`

	Note string `xml:"Note,omitempty" json:"Note,omitempty"`

	IdTransazione string `xml:"IdTransazione,omitempty" json:"IdTransazione,omitempty"`

	ListaFlussiRendicontazione *ArrayOfListaFlussiRendicontazione `xml:"ListaFlussiRendicontazione,omitempty" json:"ListaFlussiRendicontazione,omitempty"`
}

type ArrayOfListaFlussiRendicontazione struct {
	ListaFlussiRendicontazione []*ListaFlussiRendicontazione `xml:"ListaFlussiRendicontazione,omitempty" json:"ListaFlussiRendicontazione,omitempty"`
}

type RequestEstraiVersamentiAnonimi struct {
	Authentication *Authentication `xml:"Authentication,omitempty" json:"Authentication,omitempty"`

	DaDataCreazione soap.XSDDateTime `xml:"DaDataCreazione,omitempty" json:"DaDataCreazione,omitempty"`

	ADataCreazione soap.XSDDateTime `xml:"ADataCreazione,omitempty" json:"ADataCreazione,omitempty"`

	TipoPagamento string `xml:"TipoPagamento,omitempty" json:"TipoPagamento,omitempty"`
}

type ResponseEstraiVersamentiAnonimi struct {
	Esito *TipoEsito `xml:"Esito,omitempty" json:"Esito,omitempty"`

	DettagliEsito *ArrayOfDettaglioEsito `xml:"DettagliEsito,omitempty" json:"DettagliEsito,omitempty"`

	ListaStatoVersamenti *ArrayOfStatoVersamento `xml:"ListaStatoVersamenti,omitempty" json:"ListaStatoVersamenti,omitempty"`
}

type RequestParametriIuv struct {
	Authentication *Authentication `xml:"Authentication,omitempty" json:"Authentication,omitempty"`
}

type ResponseParametriIuv struct {
	Esito *TipoEsito `xml:"Esito,omitempty" json:"Esito,omitempty"`

	AuxDigit *TipoAuxDigit `xml:"AuxDigit,omitempty" json:"AuxDigit,omitempty"`

	ApplicationCode string `xml:"ApplicationCode,omitempty" json:"ApplicationCode,omitempty"`

	ConsumerCode string `xml:"ConsumerCode,omitempty" json:"ConsumerCode,omitempty"`

	Note string `xml:"Note,omitempty" json:"Note,omitempty"`
}

type RequestDettaglioDebito struct {
	Authentication *Authentication `xml:"Authentication,omitempty" json:"Authentication,omitempty"`

	CodiceIUV string `xml:"codiceIUV,omitempty" json:"codiceIUV,omitempty"`
}

type ResponseDettaglioDebito struct {
	Esito *TipoEsito `xml:"Esito,omitempty" json:"Esito,omitempty"`

	DettaglioDebito *Debito `xml:"DettaglioDebito,omitempty" json:"DettaglioDebito,omitempty"`

	Note string `xml:"Note,omitempty" json:"Note,omitempty"`
}

type RequestChiediAvvisiPagamento struct {
	Authentication *Authentication `xml:"Authentication,omitempty" json:"Authentication,omitempty"`

	ListaIUV *ArrayOfString `xml:"ListaIUV,omitempty" json:"ListaIUV,omitempty"`

	AutorizzazioneBollettino *TipoAutorizzazionePoste `xml:"AutorizzazioneBollettino,omitempty" json:"AutorizzazioneBollettino,omitempty"`
}

type ResponseChiediAvvisiPagamento struct {
	Esito *TipoEsito `xml:"Esito,omitempty" json:"Esito,omitempty"`

	AvvisiPagamento *ArrayOfDettaglioAvvisiPagamento `xml:"AvvisiPagamento,omitempty" json:"AvvisiPagamento,omitempty"`

	Note string `xml:"Note,omitempty" json:"Note,omitempty"`
}

type ArrayOfDettaglioAvvisiPagamento struct {
	DettaglioAvvisiPagamento []*DettaglioAvvisiPagamento `xml:"DettaglioAvvisiPagamento,omitempty" json:"DettaglioAvvisiPagamento,omitempty"`
}

type DettaglioAvvisiPagamento struct {
	IUV *ArrayOfString `xml:"IUV,omitempty" json:"IUV,omitempty"`

	Avviso string `xml:"Avviso,omitempty" json:"Avviso,omitempty"`

	Esito *TipoEsito `xml:"Esito,omitempty" json:"Esito,omitempty"`

	Note string `xml:"Note,omitempty" json:"Note,omitempty"`
}

type RequestChiediRicevutaTelematica struct {
	Authentication *Authentication `xml:"Authentication,omitempty" json:"Authentication,omitempty"`

	ListaIUV *ArrayOfString `xml:"ListaIUV,omitempty" json:"ListaIUV,omitempty"`

	TipologiaRicevuta *ArrayOfRicevutaTelematicaType `xml:"TipologiaRicevuta,omitempty" json:"TipologiaRicevuta,omitempty"`
}

type ArrayOfRicevutaTelematicaType struct {
	RicevutaTelematicaType []*RicevutaTelematicaType `xml:"RicevutaTelematicaType,omitempty" json:"RicevutaTelematicaType,omitempty"`
}

type ResponseChiediRicevutaTelematica struct {
	Esito *TipoEsito `xml:"Esito,omitempty" json:"Esito,omitempty"`

	RicevutePagamento *ArrayOfDettaglioRicevute `xml:"RicevutePagamento,omitempty" json:"RicevutePagamento,omitempty"`

	Note string `xml:"Note,omitempty" json:"Note,omitempty"`
}

type ArrayOfDettaglioRicevute struct {
	DettaglioRicevute []*DettaglioRicevute `xml:"DettaglioRicevute,omitempty" json:"DettaglioRicevute,omitempty"`
}

type DettaglioRicevute struct {
	Iuv string `xml:"Iuv,omitempty" json:"Iuv,omitempty"`

	Ricevute *ArrayOfDettaglioRicevutaPagamento `xml:"Ricevute,omitempty" json:"Ricevute,omitempty"`

	Esito *TipoEsito `xml:"Esito,omitempty" json:"Esito,omitempty"`

	Note string `xml:"Note,omitempty" json:"Note,omitempty"`
}

type ArrayOfDettaglioRicevutaPagamento struct {
	DettaglioRicevutaPagamento []*DettaglioRicevutaPagamento `xml:"DettaglioRicevutaPagamento,omitempty" json:"DettaglioRicevutaPagamento,omitempty"`
}

type DettaglioRicevutaPagamento struct {
	Esito *TipoEsito `xml:"Esito,omitempty" json:"Esito,omitempty"`

	Note string `xml:"Note,omitempty" json:"Note,omitempty"`

	RicevutaXML string `xml:"RicevutaXML,omitempty" json:"RicevutaXML,omitempty"`

	DataPagamento soap.XSDDateTime `xml:"DataPagamento,omitempty" json:"DataPagamento,omitempty"`

	TipologiaRicevuta *RicevutaTelematicaType `xml:"TipologiaRicevuta,omitempty" json:"TipologiaRicevuta,omitempty"`
}

type RequestChiediEstrattoConto struct {
	Authentication *Authentication `xml:"Authentication,omitempty" json:"Authentication,omitempty"`

	CodiceFiscalePartitaIvaDebitore string `xml:"CodiceFiscalePartitaIvaDebitore,omitempty" json:"CodiceFiscalePartitaIvaDebitore,omitempty"`

	TipologiaPagamento string `xml:"TipologiaPagamento,omitempty" json:"TipologiaPagamento,omitempty"`

	TipoRichiesta *TipoRichiestaEstrattoConto `xml:"TipoRichiesta,omitempty" json:"TipoRichiesta,omitempty"`

	DaDataCreazione *soap.XSDDateTime `xml:"DaDataCreazione,omitempty" json:"DaDataCreazione,omitempty"`

	ADataCreazione *soap.XSDDateTime `xml:"ADataCreazione,omitempty" json:"ADataCreazione,omitempty"`
}

type ResponseChiediEstrattoConto struct {
	Esito *TipoEsito `xml:"Esito,omitempty" json:"Esito,omitempty"`

	ListaDettagliEsiti *ArrayOfDettaglioEsito `xml:"ListaDettagliEsiti,omitempty" json:"ListaDettagliEsiti,omitempty"`

	Note string `xml:"Note,omitempty" json:"Note,omitempty"`

	Dettagli *ArrayOfDettaglioEstrattoConto `xml:"Dettagli,omitempty" json:"Dettagli,omitempty"`
}

type ArrayOfDettaglioEstrattoConto struct {
	DettaglioEstrattoConto []*DettaglioEstrattoConto `xml:"DettaglioEstrattoConto,omitempty" json:"DettaglioEstrattoConto,omitempty"`
}

type DettaglioEstrattoConto struct {
	IdDebito string `xml:"IdDebito,omitempty" json:"IdDebito,omitempty"`

	CodiceConsumer string `xml:"CodiceConsumer,omitempty" json:"CodiceConsumer,omitempty"`

	Tipologia string `xml:"Tipologia,omitempty" json:"Tipologia,omitempty"`

	DescrizioneCausaleDebito string `xml:"DescrizioneCausaleDebito,omitempty" json:"DescrizioneCausaleDebito,omitempty"`

	DataEmissione *soap.XSDDateTime `xml:"DataEmissione,omitempty" json:"DataEmissione,omitempty"`

	DataCreazione *soap.XSDDateTime `xml:"DataCreazione,omitempty" json:"DataCreazione,omitempty"`

	ImportoDebito float64 `xml:"ImportoDebito,omitempty" json:"ImportoDebito,omitempty"`

	DebitoreCodiceFiscalePartitaIva string `xml:"DebitoreCodiceFiscalePartitaIva,omitempty" json:"DebitoreCodiceFiscalePartitaIva,omitempty"`

	DebitoreCognome string `xml:"DebitoreCognome,omitempty" json:"DebitoreCognome,omitempty"`

	DebitoreNome string `xml:"DebitoreNome,omitempty" json:"DebitoreNome,omitempty"`

	IUV string `xml:"IUV,omitempty" json:"IUV,omitempty"`

	CodiceAvviso string `xml:"CodiceAvviso,omitempty" json:"CodiceAvviso,omitempty"`

	DescrizioneCausaleVersamento string `xml:"DescrizioneCausaleVersamento,omitempty" json:"DescrizioneCausaleVersamento,omitempty"`

	DataScadenza *soap.XSDDateTime `xml:"DataScadenza,omitempty" json:"DataScadenza,omitempty"`

	DataFineValidita *soap.XSDDateTime `xml:"DataFineValidita,omitempty" json:"DataFineValidita,omitempty"`

	Importo float64 `xml:"Importo,omitempty" json:"Importo,omitempty"`

	RataUnica bool `xml:"RataUnica,omitempty" json:"RataUnica,omitempty"`

	NumeroRata int32 `xml:"NumeroRata,omitempty" json:"NumeroRata,omitempty"`

	RataDescr string `xml:"RataDescr,omitempty" json:"RataDescr,omitempty"`

	Stato *TipoStatoVersamento `xml:"Stato,omitempty" json:"Stato,omitempty"`

	DescrizioneStato string `xml:"DescrizioneStato,omitempty" json:"DescrizioneStato,omitempty"`
}

type RequestChiediVersamentiPagati struct {
	Authentication *Authentication `xml:"Authentication,omitempty" json:"Authentication,omitempty"`

	DaDataPagamento soap.XSDDateTime `xml:"DaDataPagamento,omitempty" json:"DaDataPagamento,omitempty"`

	ADataPagamento *soap.XSDDateTime `xml:"ADataPagamento,omitempty" json:"ADataPagamento,omitempty"`

	Tipologia string `xml:"Tipologia,omitempty" json:"Tipologia,omitempty"`
}

type ResponseChiediVersamentiPagati struct {
	Esito *TipoEsito `xml:"Esito,omitempty" json:"Esito,omitempty"`

	DettagliEsito *ArrayOfDettaglioEsito `xml:"DettagliEsito,omitempty" json:"DettagliEsito,omitempty"`

	Note string `xml:"Note,omitempty" json:"Note,omitempty"`

	VersamentiPagati *ArrayOfStatoVersamento `xml:"VersamentiPagati,omitempty" json:"VersamentiPagati,omitempty"`
}

type Service interface {
	CaricaListaVersamenti(request *CaricaListaVersamenti) (*CaricaListaVersamentiResponse, error)

	CaricaListaVersamentiContext(ctx context.Context, request *CaricaListaVersamenti) (*CaricaListaVersamentiResponse, error)

	VerificaStatoVersamenti(request *VerificaStatoVersamenti) (*VerificaStatoVersamentiResponse, error)

	VerificaStatoVersamentiContext(ctx context.Context, request *VerificaStatoVersamenti) (*VerificaStatoVersamentiResponse, error)

	CaricamentoVersamentiOTF(request *CaricamentoVersamentiOTF) (*CaricamentoVersamentiOTFResponse, error)

	CaricamentoVersamentiOTFContext(ctx context.Context, request *CaricamentoVersamentiOTF) (*CaricamentoVersamentiOTFResponse, error)

	VerificaTransazioneOTF(request *VerificaTransazioneOTF) (*VerificaTransazioneOTFResponse, error)

	VerificaTransazioneOTFContext(ctx context.Context, request *VerificaTransazioneOTF) (*VerificaTransazioneOTFResponse, error)

	AnnullaListaVersamenti(request *AnnullaListaVersamenti) (*AnnullaListaVersamentiResponse, error)

	AnnullaListaVersamentiContext(ctx context.Context, request *AnnullaListaVersamenti) (*AnnullaListaVersamentiResponse, error)

	NotificaPagamentoSenzaRPT(request *NotificaPagamentoSenzaRPT) (*NotificaPagamentoSenzaRPTResponse, error)

	NotificaPagamentoSenzaRPTContext(ctx context.Context, request *NotificaPagamentoSenzaRPT) (*NotificaPagamentoSenzaRPTResponse, error)

	ChiediListaFlussiRendicontazione(request *ChiediListaFlussiRendicontazione) (*ChiediListaFlussiRendicontazioneResponse, error)

	ChiediListaFlussiRendicontazioneContext(ctx context.Context, request *ChiediListaFlussiRendicontazione) (*ChiediListaFlussiRendicontazioneResponse, error)

	ChiediFlussiRendicontazione(request *ChiediFlussiRendicontazione) (*ChiediFlussiRendicontazioneResponse, error)

	ChiediFlussiRendicontazioneContext(ctx context.Context, request *ChiediFlussiRendicontazione) (*ChiediFlussiRendicontazioneResponse, error)

	EstraiVersamentiAnonimi(request *EstraiVersamentiAnonimi) (*EstraiVersamentiAnonimiResponse, error)

	EstraiVersamentiAnonimiContext(ctx context.Context, request *EstraiVersamentiAnonimi) (*EstraiVersamentiAnonimiResponse, error)

	OttieniParametriIuv(request *OttieniParametriIuv) (*OttieniParametriIuvResponse, error)

	OttieniParametriIuvContext(ctx context.Context, request *OttieniParametriIuv) (*OttieniParametriIuvResponse, error)

	AggiornaListaVersamenti(request *AggiornaListaVersamenti) (*AggiornaListaVersamentiResponse, error)

	AggiornaListaVersamentiContext(ctx context.Context, request *AggiornaListaVersamenti) (*AggiornaListaVersamentiResponse, error)

	DettaglioDebito(request *DettaglioDebito) (*DettaglioDebitoResponse, error)

	DettaglioDebitoContext(ctx context.Context, request *DettaglioDebito) (*DettaglioDebitoResponse, error)

	ChiediAvvisiPagamento(request *ChiediAvvisiPagamento) (*ChiediAvvisiPagamentoResponse, error)

	ChiediAvvisiPagamentoContext(ctx context.Context, request *ChiediAvvisiPagamento) (*ChiediAvvisiPagamentoResponse, error)

	ChiediRicevutaTelematica(request *ChiediRicevutaTelematica) (*ChiediRicevutaTelematicaResponse, error)

	ChiediRicevutaTelematicaContext(ctx context.Context, request *ChiediRicevutaTelematica) (*ChiediRicevutaTelematicaResponse, error)

	ChiediEstrattoConto(request *ChiediEstrattoConto) (*ChiediEstrattoContoResponse, error)

	ChiediEstrattoContoContext(ctx context.Context, request *ChiediEstrattoConto) (*ChiediEstrattoContoResponse, error)

	ChiediVersamentiPagati(request *ChiediVersamentiPagati) (*ChiediVersamentiPagatiResponse, error)

	ChiediVersamentiPagatiContext(ctx context.Context, request *ChiediVersamentiPagati) (*ChiediVersamentiPagatiResponse, error)
}

type service struct {
	client *soap.Client
}

func NewService(client *soap.Client) Service {
	return &service{
		client: client,
	}
}

func (service *service) CaricaListaVersamentiContext(ctx context.Context, request *CaricaListaVersamenti) (*CaricaListaVersamentiResponse, error) {
	response := new(CaricaListaVersamentiResponse)
	err := service.client.CallContext(ctx, "http://services.apsystems.it/CaricaListaVersamenti", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) CaricaListaVersamenti(request *CaricaListaVersamenti) (*CaricaListaVersamentiResponse, error) {
	return service.CaricaListaVersamentiContext(
		context.Background(),
		request,
	)
}

func (service *service) VerificaStatoVersamentiContext(ctx context.Context, request *VerificaStatoVersamenti) (*VerificaStatoVersamentiResponse, error) {
	response := new(VerificaStatoVersamentiResponse)
	err := service.client.CallContext(ctx, "http://services.apsystems.it/VerificaStatoVersamenti", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) VerificaStatoVersamenti(request *VerificaStatoVersamenti) (*VerificaStatoVersamentiResponse, error) {
	return service.VerificaStatoVersamentiContext(
		context.Background(),
		request,
	)
}

func (service *service) CaricamentoVersamentiOTFContext(ctx context.Context, request *CaricamentoVersamentiOTF) (*CaricamentoVersamentiOTFResponse, error) {
	response := new(CaricamentoVersamentiOTFResponse)
	err := service.client.CallContext(ctx, "http://services.apsystems.it/CaricamentoVersamentiOTF", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) CaricamentoVersamentiOTF(request *CaricamentoVersamentiOTF) (*CaricamentoVersamentiOTFResponse, error) {
	return service.CaricamentoVersamentiOTFContext(
		context.Background(),
		request,
	)
}

func (service *service) VerificaTransazioneOTFContext(ctx context.Context, request *VerificaTransazioneOTF) (*VerificaTransazioneOTFResponse, error) {
	response := new(VerificaTransazioneOTFResponse)
	err := service.client.CallContext(ctx, "http://services.apsystems.it/VerificaTransazioneOTF", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) VerificaTransazioneOTF(request *VerificaTransazioneOTF) (*VerificaTransazioneOTFResponse, error) {
	return service.VerificaTransazioneOTFContext(
		context.Background(),
		request,
	)
}

func (service *service) AnnullaListaVersamentiContext(ctx context.Context, request *AnnullaListaVersamenti) (*AnnullaListaVersamentiResponse, error) {
	response := new(AnnullaListaVersamentiResponse)
	err := service.client.CallContext(ctx, "http://services.apsystems.it/AnnullaListaVersamenti", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) AnnullaListaVersamenti(request *AnnullaListaVersamenti) (*AnnullaListaVersamentiResponse, error) {
	return service.AnnullaListaVersamentiContext(
		context.Background(),
		request,
	)
}

func (service *service) NotificaPagamentoSenzaRPTContext(ctx context.Context, request *NotificaPagamentoSenzaRPT) (*NotificaPagamentoSenzaRPTResponse, error) {
	response := new(NotificaPagamentoSenzaRPTResponse)
	err := service.client.CallContext(ctx, "http://services.apsystems.it/NotificaPagamentoSenzaRPT", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) NotificaPagamentoSenzaRPT(request *NotificaPagamentoSenzaRPT) (*NotificaPagamentoSenzaRPTResponse, error) {
	return service.NotificaPagamentoSenzaRPTContext(
		context.Background(),
		request,
	)
}

func (service *service) ChiediListaFlussiRendicontazioneContext(ctx context.Context, request *ChiediListaFlussiRendicontazione) (*ChiediListaFlussiRendicontazioneResponse, error) {
	response := new(ChiediListaFlussiRendicontazioneResponse)
	err := service.client.CallContext(ctx, "http://services.apsystems.it/ChiediListaFlussiRendicontazione", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) ChiediListaFlussiRendicontazione(request *ChiediListaFlussiRendicontazione) (*ChiediListaFlussiRendicontazioneResponse, error) {
	return service.ChiediListaFlussiRendicontazioneContext(
		context.Background(),
		request,
	)
}

func (service *service) ChiediFlussiRendicontazioneContext(ctx context.Context, request *ChiediFlussiRendicontazione) (*ChiediFlussiRendicontazioneResponse, error) {
	response := new(ChiediFlussiRendicontazioneResponse)
	err := service.client.CallContext(ctx, "http://services.apsystems.it/ChiediFlussiRendicontazione", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) ChiediFlussiRendicontazione(request *ChiediFlussiRendicontazione) (*ChiediFlussiRendicontazioneResponse, error) {
	return service.ChiediFlussiRendicontazioneContext(
		context.Background(),
		request,
	)
}

func (service *service) EstraiVersamentiAnonimiContext(ctx context.Context, request *EstraiVersamentiAnonimi) (*EstraiVersamentiAnonimiResponse, error) {
	response := new(EstraiVersamentiAnonimiResponse)
	err := service.client.CallContext(ctx, "http://services.apsystems.it/EstraiVersamentiAnonimi", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) EstraiVersamentiAnonimi(request *EstraiVersamentiAnonimi) (*EstraiVersamentiAnonimiResponse, error) {
	return service.EstraiVersamentiAnonimiContext(
		context.Background(),
		request,
	)
}

func (service *service) OttieniParametriIuvContext(ctx context.Context, request *OttieniParametriIuv) (*OttieniParametriIuvResponse, error) {
	response := new(OttieniParametriIuvResponse)
	err := service.client.CallContext(ctx, "http://services.apsystems.it/OttieniParametriIuv", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) OttieniParametriIuv(request *OttieniParametriIuv) (*OttieniParametriIuvResponse, error) {
	return service.OttieniParametriIuvContext(
		context.Background(),
		request,
	)
}

func (service *service) AggiornaListaVersamentiContext(ctx context.Context, request *AggiornaListaVersamenti) (*AggiornaListaVersamentiResponse, error) {
	response := new(AggiornaListaVersamentiResponse)
	err := service.client.CallContext(ctx, "http://services.apsystems.it/AggiornaListaVersamenti", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) AggiornaListaVersamenti(request *AggiornaListaVersamenti) (*AggiornaListaVersamentiResponse, error) {
	return service.AggiornaListaVersamentiContext(
		context.Background(),
		request,
	)
}

func (service *service) DettaglioDebitoContext(ctx context.Context, request *DettaglioDebito) (*DettaglioDebitoResponse, error) {
	response := new(DettaglioDebitoResponse)
	err := service.client.CallContext(ctx, "http://services.apsystems.it/DettaglioDebito", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) DettaglioDebito(request *DettaglioDebito) (*DettaglioDebitoResponse, error) {
	return service.DettaglioDebitoContext(
		context.Background(),
		request,
	)
}

func (service *service) ChiediAvvisiPagamentoContext(ctx context.Context, request *ChiediAvvisiPagamento) (*ChiediAvvisiPagamentoResponse, error) {
	response := new(ChiediAvvisiPagamentoResponse)
	err := service.client.CallContext(ctx, "http://services.apsystems.it/ChiediAvvisiPagamento", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) ChiediAvvisiPagamento(request *ChiediAvvisiPagamento) (*ChiediAvvisiPagamentoResponse, error) {
	return service.ChiediAvvisiPagamentoContext(
		context.Background(),
		request,
	)
}

func (service *service) ChiediRicevutaTelematicaContext(ctx context.Context, request *ChiediRicevutaTelematica) (*ChiediRicevutaTelematicaResponse, error) {
	response := new(ChiediRicevutaTelematicaResponse)
	err := service.client.CallContext(ctx, "http://services.apsystems.it/ChiediRicevutaTelematica", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) ChiediRicevutaTelematica(request *ChiediRicevutaTelematica) (*ChiediRicevutaTelematicaResponse, error) {
	return service.ChiediRicevutaTelematicaContext(
		context.Background(),
		request,
	)
}

func (service *service) ChiediEstrattoContoContext(ctx context.Context, request *ChiediEstrattoConto) (*ChiediEstrattoContoResponse, error) {
	response := new(ChiediEstrattoContoResponse)
	err := service.client.CallContext(ctx, "http://services.apsystems.it/ChiediEstrattoConto", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) ChiediEstrattoConto(request *ChiediEstrattoConto) (*ChiediEstrattoContoResponse, error) {
	return service.ChiediEstrattoContoContext(
		context.Background(),
		request,
	)
}

func (service *service) ChiediVersamentiPagatiContext(ctx context.Context, request *ChiediVersamentiPagati) (*ChiediVersamentiPagatiResponse, error) {
	response := new(ChiediVersamentiPagatiResponse)
	err := service.client.CallContext(ctx, "http://services.apsystems.it/ChiediVersamentiPagati", request, response)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (service *service) ChiediVersamentiPagati(request *ChiediVersamentiPagati) (*ChiediVersamentiPagatiResponse, error) {
	return service.ChiediVersamentiPagatiContext(
		context.Background(),
		request,
	)
}
