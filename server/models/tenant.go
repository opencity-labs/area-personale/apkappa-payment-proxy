package models

type Tenant struct {
	ID          string `json:"id"`
	Active      bool   `json:"active"`
	Endpoint    string `json:"endpoint"`
	Production  bool   `json:"production"`
	Code        string `json:"code"`
	Consumer    string `json:"consumer"`
	IUVAux      string `json:"iuv_aux"`
	IUVApp      string `json:"iuv_app"`
	IUVConsumer string `json:"iuv_consumer"`
	Key         string `json:"key"`
}
