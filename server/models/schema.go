package models

type Schema struct {
	Display    string            `json:"display,omitempty"`
	Components []SchemaComponent `json:"components,omitempty"`
}

type SchemaComponent struct {
	Label                  string                      `json:"label,omitempty"`
	Description            string                      `json:"description,omitempty"`
	Placeholder            string                      `json:"placeholder,omitempty"`
	Spellcheck             bool                        `json:"spellcheck,omitempty"`
	Attributes             *SchemaComponentAttribute   `json:"attributes,omitempty"`
	Validate               *SchemaComponentValidate    `json:"validate,omitempty"`
	Key                    string                      `json:"key,omitempty"`
	Type                   string                      `json:"type,omitempty"`
	Input                  bool                        `json:"input,omitempty"`
	Hidden                 bool                        `json:"hidden,omitempty"`
	DefaultValue           any                         `json:"defaultValue,omitempty"`
	TableView              bool                        `json:"tableView,omitempty"`
	ShowValidations        bool                        `json:"showValidations,omitempty"`
	Reorder                bool                        `json:"reorder,omitempty"`
	AddAnotherPosition     string                      `json:"addAnotherPosition,omitempty"`
	LayoutFixed            bool                        `json:"layoutFixed,omitempty"`
	EnableRowGroups        bool                        `json:"enableRowGroups,omitempty"`
	InitEmpty              bool                        `json:"initEmpty,omitempty"`
	Conditional            *SchemaComponentConditional `json:"conditional,omitempty"`
	Components             []*SchemaComponent          `json:"components,omitempty"`
	Mask                   bool                        `json:"mask,omitempty"`
	Delimiter              bool                        `json:"delimiter,omitempty"`
	RequireDecimal         bool                        `json:"requireDecimal,omitempty"`
	InputFormat            any                         `json:"inputFormat,omitempty"`
	TruncateMultipleSpaces bool                        `json:"truncateMultipleSpaces,omitempty"`
	Widget                 string                      `json:"widget,omitempty"`
	CalculateValue         string                      `json:"calculateValue,omitempty"`
	Data                   *SchemaComponentData        `json:"data,omitempty"`
}

type SchemaComponentAttribute struct {
	Readonly string `json:"readonly,omitempty"`
}

type SchemaComponentValidate struct {
	Required bool `json:"required,omitempty"`
}

type SchemaComponentConditional struct {
	Show bool   `json:"show,omitempty"`
	When string `json:"when,omitempty"`
	Eq   string `json:"eq,omitempty"`
}

type SchemaComponentDefaultValueSplit struct {
	SplitCode   string  `json:"split_code"`
	SplitAmount float64 `json:"split_amount"`
	SplitType   string  `json:"split_type"`
	SplitKey    string  `json:"split_key"`
}

type SchemaComponentData struct {
	Values []*SchemaComponentDataValue `json:"values,omitempty"`
}

type SchemaComponentDataValue struct {
	Label string `json:"label,omitempty"`
	Value string `json:"value,omitempty"`
}

var TenantSchema = Schema{
	Display: "form",
	Components: []SchemaComponent{
		{
			Label:       "UUID del Tenant",
			Hidden:      true,
			Placeholder: "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
			Spellcheck:  false,
			Attributes: &SchemaComponentAttribute{
				Readonly: "readonly",
			},
			Validate: &SchemaComponentValidate{
				Required: true,
			},
			Key:       "id",
			Type:      "textfield",
			Input:     true,
			TableView: true,
		},
		{
			Label:        "Abilitato",
			Key:          "active",
			Type:         "checkbox",
			Input:        true,
			Hidden:       true,
			DefaultValue: true,
			TableView:    false,
		},
		{
			Label:       "Endpoint Apkappa",
			Placeholder: "Endpoint API assegnato da Apkappa",
			Validate: &SchemaComponentValidate{
				Required: true,
			},
			Key:       "endpoint",
			Type:      "textfield",
			Input:     true,
			TableView: true,
		},
		{
			Label:        "Ambiente di produzione",
			Placeholder:  "Flaggare questo campo per abilitare l'ambiente di produzione",
			Key:          "production",
			Type:         "checkbox",
			Input:        true,
			DefaultValue: false,
			TableView:    false,
		},
		{
			Label:       "Codice Apkappa",
			Placeholder: "Codice ente assegnato da Apkappa",
			Validate: &SchemaComponentValidate{
				Required: true,
			},
			Key:       "code",
			Type:      "textfield",
			Input:     true,
			TableView: true,
		},
		{
			Label:       "Consumer Apkappa",
			Placeholder: "Codice consumer assegnato da Apkappa",
			Validate: &SchemaComponentValidate{
				Required: true,
			},
			Key:       "consumer",
			Type:      "textfield",
			Input:     true,
			TableView: true,
		},
		{
			Label:       "IUV Aux digit Apkappa",
			Placeholder: "IUV aux digit assegnato da Apkappa",
			Validate: &SchemaComponentValidate{
				Required: true,
			},
			Key:       "iuv_aux",
			Type:      "textfield",
			Input:     true,
			TableView: true,
		},
		{
			Label:       "IUV Application Apkappa",
			Placeholder: "IUV application assegnato da Apkappa",
			Validate: &SchemaComponentValidate{
				Required: true,
			},
			Key:       "iuv_app",
			Type:      "textfield",
			Input:     true,
			TableView: true,
		},
		{
			Label:       "IUV Consumer Apkappa",
			Placeholder: "IUV consumer assegnato da Apkappa",
			Validate: &SchemaComponentValidate{
				Required: true,
			},
			Key:       "iuv_consumer",
			Type:      "textfield",
			Input:     true,
			TableView: true,
		},
		{
			Label:       "Chiave Apkappa",
			Placeholder: "Chiave di sicurezza assegnato da Apkappa",
			Validate: &SchemaComponentValidate{
				Required: true,
			},
			Key:       "key",
			Type:      "password",
			Input:     true,
			TableView: true,
		},
		{
			Label:           "Salva",
			ShowValidations: false,
			Key:             "submit",
			Type:            "button",
			Input:           true,
			TableView:       false,
		},
	},
}

var ServiceSchema = Schema{
	Display: "form",
	Components: []SchemaComponent{
		{
			Label:       "UUID del Servizio",
			Hidden:      true,
			Placeholder: "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
			Spellcheck:  false,
			Attributes: &SchemaComponentAttribute{
				Readonly: "readonly",
			},
			Validate: &SchemaComponentValidate{
				Required: true,
			},
			Key:       "id",
			Type:      "textfield",
			Input:     true,
			TableView: true,
		},
		{
			Label:       "UUID del Tenant",
			Hidden:      true,
			Placeholder: "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
			Spellcheck:  false,
			Attributes: &SchemaComponentAttribute{
				Readonly: "readonly",
			},
			Validate: &SchemaComponentValidate{
				Required: true,
			},
			Key:       "tenant_id",
			Type:      "textfield",
			Input:     true,
			TableView: true,
		},
		{
			Label:        "Abilitato",
			Hidden:       true,
			DefaultValue: true,
			Key:          "active",
			Type:         "checkbox",
			Input:        true,
			TableView:    false,
		},
		{
			Label:       "Codice servizio",
			Placeholder: "PAGOPA04",
			Description: "Questo codice è fornito all'ente da APKAPPA",
			Validate: &SchemaComponentValidate{
				Required: true,
			},
			Key:       "code",
			Type:      "textfield",
			Input:     true,
			TableView: true,
		},
		{
			Label:       "Nome tributo",
			Placeholder: "PAGOPA04",
			Description: "Questo nome è fornito all'ente da APKAPPA",
			Validate: &SchemaComponentValidate{
				Required: true,
			},
			Key:       "name",
			Type:      "textfield",
			Input:     true,
			TableView: true,
		},
		{
			Label:       "Descrizione del servizio",
			Placeholder: "Contributo Pratiche Polizia Municipale",
			Validate: &SchemaComponentValidate{
				Required: true,
			},
			Key:       "description",
			Type:      "textfield",
			Input:     true,
			TableView: true,
		},
		{
			Label:        "Bilancio",
			DefaultValue: true,
			Key:          "splitted",
			Type:         "checkbox",
			Input:        true,
			TableView:    true,
		},
		{
			Label:              "Dettagli bilancio",
			Reorder:            false,
			AddAnotherPosition: "bottom",
			LayoutFixed:        false,
			EnableRowGroups:    false,
			InitEmpty:          true,
			Conditional: &SchemaComponentConditional{
				Show: true,
				When: "splitted",
				Eq:   "true",
			},
			DefaultValue: []*SchemaComponentDefaultValueSplit{
				{
					SplitCode:   "c_1",
					SplitAmount: 0,
					SplitType:   "CAP",
					SplitKey:    "c_1",
				},
			},
			Key:       "split",
			Type:      "datagrid",
			Input:     true,
			TableView: true,
			Components: []*SchemaComponent{
				{
					Label:       "Codice",
					Placeholder: "c_1",
					Description: "Identificativo univoco della voce di bilancio. Testo libero",
					Validate: &SchemaComponentValidate{
						Required: true,
					},
					Key:       "split_code",
					Type:      "textfield",
					Input:     true,
					TableView: true,
				},
				{
					Label:       "Importo",
					Placeholder: "16.00",
					Description: "Importo della voce di bilancio. NB: La somma degli importi delle voci DEVE equivalere all'importo totale",
					Validate: &SchemaComponentValidate{
						Required: true,
					},
					Mask:                   false,
					Delimiter:              false,
					RequireDecimal:         false,
					InputFormat:            "plain",
					TruncateMultipleSpaces: false,
					Key:                    "split_amount",
					Type:                   "number",
					Input:                  true,
					TableView:              true,
				},
				{
					Label:       "Tipo",
					Description: "Tipologia della voce di bilancio",
					Validate: &SchemaComponentValidate{
						Required: true,
					},
					Data: &SchemaComponentData{
						Values: []*SchemaComponentDataValue{
							{
								Label: "Capitolo",
								Value: "CAP",
							},
							{
								Label: "Accertamento",
								Value: "ACC",
							},
						},
					},
					DefaultValue: "CAP",
					Key:          "split_type",
					Type:         "select",
					Widget:       "html5",
					Input:        true,
					TableView:    true,
				},
				{
					Label:       "Key",
					Placeholder: "c_1",
					Description: "Chiave univoca della voce di bilancio da fornire ad APKAPPA. Testo libero",
					Validate: &SchemaComponentValidate{
						Required: true,
					},
					Key:       "split_key",
					Type:      "textfield",
					Input:     true,
					TableView: true,
				},
			},
		},
		{
			Label:          "hidden",
			CalculateValue: "if (!data.split || data.split == 'undefined') {\n  data.split = []\n} else if (typeof data.split==='object' && Object.keys(data.split).length === 0) {\n  data.split = [];\n}",
			Key:            "hidden",
			Type:           "hidden",
			Input:          true,
			TableView:      false,
		},
		{
			Label:           "Salva",
			ShowValidations: false,
			Key:             "submit",
			Type:            "button",
			Input:           true,
			TableView:       false,
		},
	},
}
