package server

import (
	"encoding/base64"
	"time"

	"github.com/hooklift/gowsdl/soap"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/opencontent/stanza-del-cittadino/apkappa-payment-proxy/server/models"
	"gitlab.com/opencontent/stanza-del-cittadino/apkappa-payment-proxy/server/models/apkappa"
)

type FlowProviderReceiptPayment struct {
	Name     string
	Sctx     *ServerContext
	Payment  *models.Payment
	Service  *models.Service
	Tenant   *models.Tenant
	Request  *apkappa.ChiediRicevutaTelematica
	Client   *soap.Client
	Apkappa  apkappa.Service
	Err      error
	Msg      string
	Status   bool
	Result   *apkappa.ChiediRicevutaTelematicaResponse
	Received bool
	Receipt  []byte
}

func (flow *FlowProviderReceiptPayment) Exec() bool {
	flow.Name = "ProviderReceiptPayment"

	flow.Status = true &&
		flow.prepareRequest() &&
		flow.doRequest()

	return flow.Status
}

func (flow *FlowProviderReceiptPayment) prepareRequest() bool {
	iuvCode := flow.Payment.Payment.IUV

	authenticationHash := apkappa.GetAuthenticationHash(iuvCode + flow.Tenant.Key)

	ricevutaType := apkappa.RicevutaTelematicaTypePdf

	flow.Request = &apkappa.ChiediRicevutaTelematica{
		RequestData: &apkappa.RequestChiediRicevutaTelematica{
			Authentication: &apkappa.Authentication{
				Ente:           flow.Tenant.Code,
				CodiceServizio: flow.Service.Code,
				CodiceConsumer: flow.Tenant.Consumer,
				Timestamp:      soap.CreateXsdDateTime(time.Now(), false),
				Hash:           authenticationHash,
			},
			ListaIUV: &apkappa.ArrayOfString{
				Astring: []*string{
					&iuvCode,
				},
			},
			TipologiaRicevuta: &apkappa.ArrayOfRicevutaTelematicaType{
				RicevutaTelematicaType: []*apkappa.RicevutaTelematicaType{
					&ricevutaType,
				},
			},
		},
	}

	// fmt.Println("IUV: ", iuvCode)
	// fmt.Println("HASH: ", authenticationHash)
	// fmt.Println("REQ: ", flow.Request)

	return true
}

func (flow *FlowProviderReceiptPayment) doRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("payment_receipt"))

	flow.Client = soap.NewClient(flow.Tenant.Endpoint)

	flow.Apkappa = apkappa.NewService(flow.Client)

	var err error
	flow.Result, err = flow.Apkappa.ChiediRicevutaTelematica(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting notice request"

		MetricsPaymentsProviderError.WithLabelValues("payment_receipt").Inc()

		return false
	}

	requestResult := *flow.Result.ChiediRicevutaTelematicaResult.Esito == apkappa.TipoEsitoOK
	if !requestResult {
		flow.Msg = "provider response ko"

		MetricsPaymentsProviderError.WithLabelValues("payment_receipt").Inc()

		// fmt.Println("ERROR2: ", flow.Result.ChiediRicevutaTelematicaResult)
		return false
	}

	var receiptState apkappa.TipoEsito
	var receipt string
	if flow.Result.ChiediRicevutaTelematicaResult.RicevutePagamento != nil {
		for _, dettaglioRicevutaPagamento := range flow.Result.ChiediRicevutaTelematicaResult.RicevutePagamento.DettaglioRicevute {
			if dettaglioRicevutaPagamento.Ricevute != nil {
				for _, ricevutaPagamento := range dettaglioRicevutaPagamento.Ricevute.DettaglioRicevutaPagamento {
					receiptState = *ricevutaPagamento.Esito
					receipt = ricevutaPagamento.RicevutaXML
				}
			}

		}
	}

	if receiptState != apkappa.TipoEsitoOK {
		flow.Msg = "provider say invalid payment"

		MetricsPaymentsProviderError.WithLabelValues("payment_receipt").Inc()

		return false
	}

	timer.ObserveDuration()

	flow.Receipt, err = base64.StdEncoding.DecodeString(receipt)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while decoding notice"

		MetricsPaymentsProviderError.WithLabelValues("payment_receipt").Inc()

		return false
	}

	flow.Received = true

	return true
}
