package server

import (
	"encoding/base64"
	"time"

	"github.com/hooklift/gowsdl/soap"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/opencontent/stanza-del-cittadino/apkappa-payment-proxy/server/models"
	"gitlab.com/opencontent/stanza-del-cittadino/apkappa-payment-proxy/server/models/apkappa"
)

type FlowProviderNoticePayment struct {
	Name     string
	Sctx     *ServerContext
	Payment  *models.Payment
	Service  *models.Service
	Tenant   *models.Tenant
	Request  *apkappa.ChiediAvvisiPagamento
	Client   *soap.Client
	Apkappa  apkappa.Service
	Err      error
	Msg      string
	Status   bool
	Result   *apkappa.ChiediAvvisiPagamentoResponse
	Received bool
	Notice   []byte
}

func (flow *FlowProviderNoticePayment) Exec() bool {
	flow.Name = "ProviderNoticePayment"

	flow.Status = true &&
		flow.prepareRequest() &&
		flow.doRequest()

	return flow.Status
}

func (flow *FlowProviderNoticePayment) prepareRequest() bool {
	iuvCode := flow.Payment.Payment.IUV

	authenticationHash := apkappa.GetAuthenticationHash(iuvCode + flow.Tenant.Key)

	autorizzazioneBollettino := apkappa.TipoAutorizzazionePostePDF

	flow.Request = &apkappa.ChiediAvvisiPagamento{
		RequestData: &apkappa.RequestChiediAvvisiPagamento{
			Authentication: &apkappa.Authentication{
				Ente:           flow.Tenant.Code,
				CodiceServizio: flow.Service.Code,
				CodiceConsumer: flow.Tenant.Consumer,
				Timestamp:      soap.CreateXsdDateTime(time.Now(), false),
				Hash:           authenticationHash,
			},
			ListaIUV: &apkappa.ArrayOfString{
				Astring: []*string{
					&iuvCode,
				},
			},
			AutorizzazioneBollettino: &autorizzazioneBollettino,
		},
	}

	return true
}

func (flow *FlowProviderNoticePayment) doRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("payment_notice"))

	flow.Client = soap.NewClient(flow.Tenant.Endpoint)

	flow.Apkappa = apkappa.NewService(flow.Client)

	var err error
	flow.Result, err = flow.Apkappa.ChiediAvvisiPagamento(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting notice request"

		MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

		return false
	}

	requestResult := *flow.Result.ChiediAvvisiPagamentoResult.Esito == apkappa.TipoEsitoOK
	if !requestResult {
		flow.Msg = "provider response ko"

		MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

		return false
	}

	var noticeState apkappa.TipoEsito
	var notice string
	if flow.Result.ChiediAvvisiPagamentoResult.AvvisiPagamento != nil {
		for _, avvisoPagamento := range flow.Result.ChiediAvvisiPagamentoResult.AvvisiPagamento.DettaglioAvvisiPagamento {
			noticeState = *avvisoPagamento.Esito
			notice = avvisoPagamento.Avviso
		}
	}

	if noticeState != apkappa.TipoEsitoOK {
		flow.Msg = "provider say invalid payment"

		MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

		return false
	}

	timer.ObserveDuration()

	flow.Notice, err = base64.StdEncoding.DecodeString(notice)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while decoding notice"

		MetricsPaymentsProviderError.WithLabelValues("payment_notice").Inc()

		return false
	}

	flow.Received = true

	return true
}
