package server

import (
	"context"

	uuid "github.com/satori/go.uuid"
	"github.com/swaggest/usecase"
	"github.com/swaggest/usecase/status"
	"gitlab.com/opencontent/stanza-del-cittadino/apkappa-payment-proxy/server/models"
)

func GetTenantSchema(sctx *ServerContext) usecase.Interactor {
	uc := usecase.NewInteractor(func(_ context.Context, _ struct{}, output *models.Schema) error {
		*output = models.TenantSchema

		return nil
	})

	uc.SetTitle("Get Tenant Form Schema")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	// uc.SetExpectedErrors(status.Internal)

	return uc
}

type getTenantByIDInput struct {
	TenantId string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func GetTenantByID(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	tenantsSync := sctx.TenantsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input getTenantByIDInput, output *models.Tenant) error {
		tenantsSync.RLock()
		defer tenantsSync.RUnlock()

		tenant, err := tenantsCache.Get(ctx, input.TenantId)

		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get tenant by id error for: " + input.TenantId)
			return status.Internal
		}

		*output = *tenant

		return nil
	})

	uc.SetTitle("Get Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type createTenantInput struct {
	ID          string `json:"id" description:"..." required:"true"`
	Active      bool   `json:"active" description:"..." required:"true"`
	Endpoint    string `json:"endpoint" description:"..." required:"true"`
	Production  bool   `json:"production" description:"..." required:"true"`
	Code        string `json:"code" description:"..." required:"true"`
	Consumer    string `json:"consumer" description:"..." required:"true"`
	IUVAux      string `json:"iuv_aux" description:"..." required:"true"`
	IUVApp      string `json:"iuv_app" description:"..." required:"true"`
	IUVConsumer string `json:"iuv_consumer" description:"..." required:"true"`
	Key         string `json:"key" description:"..." required:"true"`
}

func CreateTenant(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	tenantsSync := sctx.TenantsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input createTenantInput, output *models.Tenant) error {
		tenantsSync.Lock()
		defer tenantsSync.Unlock()

		_, err := tenantsCache.Get(ctx, input.ID)

		if err == nil || err.Error() != "value not found in store" {
			return status.AlreadyExists
		}

		_, err = uuid.FromString(input.ID)
		if err != nil {
			return status.InvalidArgument
		}

		output.ID = input.ID
		output.Active = input.Active
		output.Endpoint = input.Endpoint
		output.Production = input.Production
		output.Code = input.Code
		output.Consumer = input.Consumer
		output.IUVAux = input.IUVAux
		output.IUVApp = input.IUVApp
		output.IUVConsumer = input.IUVConsumer
		output.Key = input.Key

		err = StoreTenant(sctx, output)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Save Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type updateTenantInput struct {
	TenantId    string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	ID          string `json:"id" description:"..." required:"true"`
	Active      bool   `json:"active" description:"..." required:"true"`
	Endpoint    string `json:"endpoint" description:"..." required:"true"`
	Production  bool   `json:"production" description:"..." required:"true"`
	Code        string `json:"code" description:"..." required:"true"`
	Consumer    string `json:"consumer" description:"..." required:"true"`
	IUVAux      string `json:"iuv_aux" description:"..." required:"true"`
	IUVApp      string `json:"iuv_app" description:"..." required:"true"`
	IUVConsumer string `json:"iuv_consumer" description:"..." required:"true"`
	Key         string `json:"key" description:"..." required:"true"`
}

func UpdateTenant(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	tenantsSync := sctx.TenantsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input updateTenantInput, output *models.Tenant) error {
		tenantsSync.Lock()
		defer tenantsSync.Unlock()

		tenant, err := tenantsCache.Get(ctx, input.TenantId)

		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get tenant by id error for: " + input.TenantId)
			return status.Internal
		}

		if input.ID != tenant.ID {
			return status.InvalidArgument
		}

		output.ID = input.ID
		output.Active = input.Active
		output.Endpoint = input.Endpoint
		output.Production = input.Production
		output.Code = input.Code
		output.Consumer = input.Consumer
		output.IUVAux = input.IUVAux
		output.IUVApp = input.IUVApp
		output.IUVConsumer = input.IUVConsumer
		output.Key = input.Key

		err = StoreTenant(sctx, output)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Update Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type patchTenantInput struct {
	TenantId    string  `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
	Active      *bool   `json:"active" description:"..."`
	Endpoint    *string `json:"endpoint" description:"..."`
	Production  *bool   `json:"production" description:"..."`
	Code        *string `json:"code" description:"..."`
	Consumer    *string `json:"consumer" description:"..."`
	IUVAux      *string `json:"iuv_aux" description:"..."`
	IUVApp      *string `json:"iuv_app" description:"..."`
	IUVConsumer *string `json:"iuv_consumer" description:"..."`
	Key         *string `json:"key" description:"..."`
}

func PatchTenant(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	tenantsSync := sctx.TenantsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input patchTenantInput, output *models.Tenant) error {
		tenantsSync.Lock()
		defer tenantsSync.Unlock()

		tenant, err := tenantsCache.Get(ctx, input.TenantId)

		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get tenant by id error for: " + input.TenantId)
			return status.Internal
		}

		output.ID = tenant.ID

		if input.Active == nil {
			output.Active = tenant.Active
		} else {
			output.Active = *input.Active
		}
		if input.Endpoint == nil {
			output.Endpoint = tenant.Endpoint
		} else {
			output.Endpoint = *input.Endpoint
		}
		if input.Production == nil {
			output.Production = tenant.Production
		} else {
			output.Production = *input.Production
		}
		if input.Code == nil {
			output.Code = tenant.Code
		} else {
			output.Code = *input.Code
		}
		if input.Consumer == nil {
			output.Consumer = tenant.Consumer
		} else {
			output.Consumer = *input.Consumer
		}
		if input.IUVAux == nil {
			output.IUVAux = tenant.IUVAux
		} else {
			output.IUVAux = *input.IUVAux
		}
		if input.IUVApp == nil {
			output.IUVApp = tenant.IUVApp
		} else {
			output.IUVApp = *input.IUVApp
		}
		if input.IUVConsumer == nil {
			output.IUVConsumer = tenant.IUVConsumer
		} else {
			output.IUVConsumer = *input.IUVConsumer
		}
		if input.Key == nil {
			output.Key = tenant.Key
		} else {
			output.Key = *input.Key
		}

		err = StoreTenant(sctx, output)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Update Existing Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}

type disableTenantInput struct {
	TenantId string `path:"tenant_id" description:"..." example:"b212c4b4-db26-4404-8c7c-47dab99dd2e6"`
}

func DisableTenant(sctx *ServerContext) usecase.Interactor {
	tenantsCache := sctx.TenantsCache()
	tenantsSync := sctx.TenantsSync()

	uc := usecase.NewInteractor(func(ctx context.Context, input disableTenantInput, _ *struct{}) error {
		tenantsSync.Lock()
		defer tenantsSync.Unlock()

		tenant, err := tenantsCache.Get(ctx, input.TenantId)

		if err != nil && err.Error() == "value not found in store" {
			return status.NotFound
		}
		if err != nil {
			sctx.LogHttpError().Stack().Err(err).Msg("get tenant by id error for: " + input.TenantId)
			return status.Internal
		}

		tenant.Active = false

		err = StoreTenant(sctx, tenant)
		if err != nil && err.Error() == "invalid data" {
			return status.InvalidArgument
		}
		if err != nil {
			return err
		}

		return nil
	})

	uc.SetTitle("Disable Tenant Configuration")
	uc.SetDescription("...")
	uc.SetTags("Tenants")
	uc.SetExpectedErrors(status.InvalidArgument)

	return uc
}
