package server

import (
	"time"

	"github.com/hooklift/gowsdl/soap"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/opencontent/stanza-del-cittadino/apkappa-payment-proxy/server/models"
	"gitlab.com/opencontent/stanza-del-cittadino/apkappa-payment-proxy/server/models/apkappa"
)

type FlowProviderOnlinePayment struct {
	Name    string
	Sctx    *ServerContext
	Payment *models.Payment
	Service *models.Service
	Tenant  *models.Tenant
	Request *apkappa.CaricamentoVersamentiOTF
	Client  *soap.Client
	Apkappa apkappa.Service
	Err     error
	Msg     string
	Status  bool
	Result  *apkappa.CaricamentoVersamentiOTFResponse
	Url     string
}

func (flow *FlowProviderOnlinePayment) Exec() bool {
	flow.Name = "ProviderOnlinePayment"

	flow.Status = true &&
		flow.prepareRequest() &&
		flow.doRequest()

	return flow.Status
}

func (flow *FlowProviderOnlinePayment) prepareRequest() bool {
	serverConfig := flow.Sctx.ServerConfig()

	iuvCode := flow.Payment.Payment.IUV

	authenticationHash := apkappa.GetAuthenticationHash(iuvCode + flow.Tenant.Key)

	var address string
	address += flow.Payment.Payer.StreetName + ", " + flow.Payment.Payer.BuildingNumber
	address += " - " + flow.Payment.Payer.PostalCode + " " + flow.Payment.Payer.TownName
	address += " (" + flow.Payment.Payer.CountrySubdivision + ") "
	address += flow.Payment.Payer.Country

	flow.Request = &apkappa.CaricamentoVersamentiOTF{
		RequestData: &apkappa.RequestCaricamentoVersamentiOTF{
			Authentication: &apkappa.Authentication{
				Ente:           flow.Tenant.Code,
				CodiceServizio: flow.Service.Code,
				CodiceConsumer: flow.Tenant.Consumer,
				Timestamp:      soap.CreateXsdDateTime(time.Now(), false),
				Hash:           authenticationHash,
			},
			Url: &apkappa.URLRedirect{
				URLDone:  serverConfig.ExternalApiUrl + serverConfig.BasePath + "landing/" + flow.Payment.ID,
				URLError: serverConfig.ExternalApiUrl + serverConfig.BasePath + "landing/" + flow.Payment.ID,
				URLBack:  serverConfig.ExternalApiUrl + serverConfig.BasePath + "landing/" + flow.Payment.ID,
			},
			ListaDebiti: &apkappa.ArrayOfDebitoOTF{
				DebitoOTF: []*apkappa.DebitoOTF{
					{
						DebitoreCodiceFiscalePartitaIva: flow.Payment.Payer.TaxIdentificationNumber,
						DebitoreCognome:                 flow.Payment.Payer.FamilyName,
						DebitoreNome:                    flow.Payment.Payer.Name,
						DebitoreEMail:                   flow.Payment.Payer.Email,
						DebitoreIndirizzo:               address,
						DebitoreIndirizzo2:              "",
						Id:                              iuvCode,
						Tipologia:                       flow.Service.Name,
						DescrizioneCausale:              flow.Service.Description,
						OggettoDelPagamento:             flow.Payment.Reason,
						Importo:                         flow.Payment.Payment.Amount,
						CodiceIUV:                       iuvCode,
						CodiceAvviso:                    flow.Tenant.IUVAux + iuvCode,
						DataScadenza:                    soap.CreateXsdDateTime(flow.Payment.Payment.ExpireAt.Time, false),
						CodiceConsumer:                  flow.Tenant.Consumer,
					},
				},
			},
		},
	}

	return true
}

func (flow *FlowProviderOnlinePayment) doRequest() bool {
	timer := prometheus.NewTimer(MetricsPaymentsProviderLatency.WithLabelValues("payment_online"))

	flow.Client = soap.NewClient(flow.Tenant.Endpoint)

	flow.Apkappa = apkappa.NewService(flow.Client)

	var err error
	flow.Result, err = flow.Apkappa.CaricamentoVersamentiOTF(flow.Request)
	if err != nil {
		flow.Err = err
		flow.Msg = "errors while requesting due request"

		MetricsPaymentsProviderError.WithLabelValues("payment_online").Inc()

		return false
	}

	requestResult := *flow.Result.CaricamentoVersamentiOTFResult.Esito == apkappa.TipoEsitoOK
	if !requestResult {
		flow.Msg = "provider response ko"

		MetricsPaymentsProviderError.WithLabelValues("payment_online").Inc()

		return false
	}

	if flow.Result.CaricamentoVersamentiOTFResult.Url == "" {
		flow.Msg = "provider did not give us a URL for payment"

		MetricsPaymentsProviderError.WithLabelValues("payment_online").Inc()

		return false
	}

	timer.ObserveDuration()

	flow.Url = flow.Result.CaricamentoVersamentiOTFResult.Url

	return true
}
